#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <stdarg.h>

int energija = 42, aa = -1;
char *izpisujem = ".";

void log_printf(const char *format, ...) {
  time_t timestamp = time(NULL);
  if(timestamp == -1)
    exit(1);

  struct tm *cal_time = localtime(&timestamp);

  // 2011-5-1 13:42:57
  int chars = printf("%d-%d-%d %d:%d:%d",
    cal_time->tm_year + 1900,
    cal_time->tm_mon + 1,
    cal_time->tm_mday,
    cal_time->tm_hour,
    cal_time->tm_min,
    cal_time->tm_sec);

  for (int i = chars; i < 30; ++i) {
    putc(' ', stdout);
  }

  // print the formatted string
  va_list vargs;
  va_start(vargs, format);
  vprintf(format, vargs);
  printf("\n");
  va_end(vargs);
}

void usr2_handler(int sig) {
  int child_pid = fork();
  aa = child_pid;
  if (child_pid == 0) {
    // CHILD
    int sleep_time = (energija % 7) + 1;
    log_printf("Sleep time: %d.", sleep_time);
    sleep(sleep_time);

    int izhodni_status = (42 * energija) % 128;
    log_printf("Child exit with status: %d.", izhodni_status);
    exit(izhodni_status);
  } else if (child_pid > 0) {
    // PARENT
    log_printf("Forked child %d.", child_pid);
  } else {
    // ERROR
    log_printf("ERROR forking!");
  }
}

void usr1_handler(int sig) {
  if (izpisujem[0] == '.') izpisujem = "*";
  else izpisujem = ".";
  log_printf("Sedaj izpisujem: %s", izpisujem);
}

void term_handler(int sig) {
  energija += 10;
  log_printf("Captured SIGTERM, increasing energy (%d).", energija);
}

void chld_handler(int sig) {
  int status;
  wait(&status);

  if (WIFEXITED(status))
    status = WEXITSTATUS(status);

  log_printf("Zombie caught with status: %d", status);
}

void kill_handler(int sig) {
  log_printf("Received kill signal, exiting.");
  exit(0);
}

void daemonize(char *name) {
  if(getppid() == 1)
    return;

  int pid = fork();
  if (pid < 0) exit(1);
  else if (pid > 0) exit(0);

  // only child (daemon) continues
  printf("Daemon PID: %d.\n", getpid());
  setsid();

  // close stdin
  close(0);
  // close any fds larger than 2 (we redirect stdout and stderr)
  for(int i = getdtablesize(); i > 2; --i)
    close(i);
  
  // redirect stdout and stderr to file
  char str[64];
  sprintf(str, "%s.log", name);
  int log_fd = open(str, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (log_fd < 0)
    exit(1);
  else {
    dup2(log_fd, 1);
    dup2(log_fd, 2);
  }

  // create pid file
  sprintf(str, "%s.pid", name);
  FILE *pid_file = fopen(str, "w");
  if(!pid_file)
    exit(1);
  fprintf(pid_file, "%d", getpid());
  fclose(pid_file);
}

int main(int argc, char **argv) {
  if (argc > 1) sscanf(argv[1], "%d", &energija);

  char program_name[strlen(argv[0]) + 1];
  sscanf(argv[0]+2, "%[^.]s", program_name);
  daemonize(program_name);

  // Daemon now
  log_printf("My PID: %d.", getpid());
  log_printf("Starting with energy: %d.", energija);

  signal(SIGTERM, term_handler);
  signal(SIGUSR1, usr1_handler);
  signal(SIGUSR2, usr2_handler);
  signal(SIGCHLD, chld_handler);
  signal(SIGSTOP, kill_handler);
  signal(SIGKILL, kill_handler);
  while (energija > 0) {
    sleep(1);
    energija -= 1;
    log_printf(izpisujem);
  }

  log_printf("Out of energy.");
}