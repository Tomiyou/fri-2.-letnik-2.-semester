#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** args) {
  char* vhod = args[1];
  char* izhod = args[2];
  char* program = args[3];

  printf("%d\n", argc);

  if (strcmp(vhod, "-") != 0) {
    // printf("Vhod: %s\n", vhod);
    int fd_vhod = open(vhod, O_RDONLY | O_CREAT);
    dup2(fd_vhod, 0);
  } else {
    // printf("Vhod: %s je enak \"-\"\n", vhod);
  }

  if (strcmp(izhod, "-") != 0) {
    // printf("Izhod: %s\n", izhod);
    int fd_izhod = open(izhod, O_WRONLY | O_CREAT | O_TRUNC);
    dup2(fd_izhod, 1);
  } else {
    // printf("Izhod: %s je enak \"-\"\n", izhod);
  }

  char** sub_args = args + 3;
  // printf("Program: %s, 0: %s, 1: %s\n", program, sub_args[0], sub_args[1]);
  execvp(program, sub_args);

  return 0;
}