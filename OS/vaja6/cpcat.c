#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
 #include <errno.h>
 #include <stdlib.h>
// Samo za perror()
#include <stdio.h>

int handle_err(char* str) {
  perror(str);
  return errno;
}

int main(int argc, char** args) {
  // preusmerimo vhod
  if (argc > 1 && strcmp(args[1], "-") != 0) {
    int fd = open(args[1], O_RDONLY);
    if (fd == -1 || dup2(fd, 0) == -1) {
      return handle_err("Prišlo je do napake pri preusmerjanju vhoda");
    }
  }

  // preusmerimo izhod
  if (argc > 2 && strcmp(args[2], "-") != 0) {
    int fd = open(args[2], O_WRONLY);
    if (fd == -1 || dup2(fd, 1) == -1) {
      return handle_err("Prišlo je do napake pri preusmerjanju izhoda");
    }
  }

  // vhod ima shared lock, izhod ima exclusive
  // ce je katerakoli datoteka ze zaklenjena, pa program caka, dokler ne dobi dostopa
  if (flock(0, LOCK_SH) == -1) {
    return handle_err("Napaka pri zaklepanju vhoda");
  }
  if (flock(1, LOCK_EX) == -1) {
    return handle_err("Napaka pri zaklepanju izhoda");
  }

  // "ne uprabljate copy&paste za vsak nacin uporabe argumentov"
  // ne razumem kaj naj bi to pomenilo, vedno je treba bite iz vhoda
  // kopirati na izhod
  char data[128];
  int bytes;
  while ((bytes = read(0, data, 128)) > 0) {
    write(1, data, bytes);
  }

  // obe kljucavnici se avtomatsko sprostita, ko se proces konca
  // torej ni potrebno izvesti sistemeskega klica za sprostitev

  return errno;
}