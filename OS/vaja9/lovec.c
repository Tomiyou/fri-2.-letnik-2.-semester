#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <wait.h>

int energija = 42, aa = -1;
char izpisujem = '.';

void usr2_handler(int sig) {
  int child_pid = fork();
  aa = child_pid;
  if (child_pid == 0) {
    // CHILD
    int sleep_time = (energija % 7) + 1;
    printf("Sleep time: %d.\n", sleep_time);
    sleep(sleep_time);

    int izhodni_status = (42 * energija) % 128;
    printf("Child exit with status: %d.\n", izhodni_status);
    exit(izhodni_status);
  } else if (child_pid > 0) {
    // PARENT
    printf("Forked child %d.", child_pid);
  } else {
    // ERROR
    printf("ERROR forking!");
  }
}

void usr1_handler(int sig) {
  if (izpisujem == '.') izpisujem = '*';
  else izpisujem = '.';
  printf("Sedaj izpisujem: %c\n", izpisujem);
}

void term_handler(int sig) {
  energija += 10;
  printf("Captured SIGTERM, increasing energy (%d).\n", energija);
}

void chld_handler(int sig) {
  int status;
  wait(&status);

  if (WIFEXITED(status))
    status = WEXITSTATUS(status);

  printf("Zombie caught with status: %d\n", status);
}

int main(int argc, char **argv) {
  if (argc > 1) sscanf(argv[1], "%d", &energija);
  printf("My PID: %d.\n", getpid());
  printf("Starting with energy: %d.\n", energija);

  signal(SIGTERM, term_handler);
  signal(SIGUSR1, usr1_handler);
  signal(SIGUSR2, usr2_handler);
  signal(SIGCHLD, chld_handler);
  while (energija > 0) {
    sleep(1);
    energija -= 1;
    putc(izpisujem, stdout);
  }

  printf("Out of energy.\n");
}