function loop {
  local print_cmd=true
  local print_mem=true
  local print_cpu=true
  local print_usr=true
  local cmd=""

  while true; do
    cmd="pid"
    [[ $print_cmd == true ]] && cmd="$cmd,comm"
    [[ $print_mem == true ]] && cmd="$cmd,%mem"
    [[ $print_cpu == true ]] && cmd="$cmd,%cpu"
    [[ $print_usr == true ]] && cmd="$cmd,user"
    ps -eo "$cmd" --sort=-pcpu | head -n 11

    if IFS="" read -n 1 -t 1 input; then
      case "$input" in
        "q")
          break;
          ;;
        "h")
          echo "Help!"
          ;;
        "c")
          [[ $print_cmd == true ]] && print_cmd=false || print_cmd=true
          ;;
        "m")
          [[ $print_mem == true ]] && print_mem=false || print_mem=true
          ;;
        "p")
          [[ $print_cpu == true ]] && print_cpu=false || print_cpu=true
          ;;
        "u")
          [[ $print_usr == true ]] && print_usr=false || print_usr=true
          ;;
        "")
          printf "\e[F"
          ;;
      esac
    fi

    printf "\e[11F\e[J"
  done
}

loop
echo ""