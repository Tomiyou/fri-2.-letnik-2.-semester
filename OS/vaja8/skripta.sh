N=100
[[ $# -ne 0 ]] && N=$1

echo "Ustvarjam $N procesov..."

for (( i=0; i<$N; i++ )); do
  if (( (1 + RANDOM % 100) < 31 )); then
    xeyes &
  else
    xclock &
  fi
  pids+=("$!")
done

echo "PIDs: ${pids[@]}"
read -n1 throwaway
printf "\e[GUkinjam procese...\n"

echo -n "KILLs: "
for (( idx=${#pids[@]}-1; idx >= 0; idx-- )); do
  kill "${pids[idx]}"
  echo -n "${pids[idx]} "
done

printf "\nProcesi končani.\n"