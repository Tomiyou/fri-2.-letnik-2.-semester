function izhod = naloga4(vhod,Fs)
% Funkcija naloga4 skusa poiskati akord v zvocnem zapisu.
%
% vhod  - vhodni zvocni zapis (vrsticni vektor tipa double) 
% Fs    - frekvenca vzorcenja
% izhod - ime akorda, ki se skriva v zvocnem zapisu (niz);
%         ce frekvence v zvocnem zapisu ne ustrezajo nobenemu
%         od navedenih akordov, vrnemo prazen niz [].

note = {"C1"; "CIS1"; "D1"; "DIS1"; "E1"; "F1"; "FIS1"; "G1"; "GIS1"; "A1"; "B1"; "H1"; "C2"; "CIS2"; "D2"; "DIS2"; "E2"; "F2"; "FIS2"; "G2"; "GIS2"; "A2"; "B2"; "H2"};
frekvence = [261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392, 415.3, 440, 466.16, 493.88, 523.25, 554.37, 587.33, 622.25, 659.25, 698.46, 739.99, 783.99, 830.61, 880, 932.33, 987.77];
graf = fft(vhod);
[graf, dolzina, delta] = removeNoise(graf, Fs);
limit = 0;
ton = 0;
akord = [];

while limit < 15
  [_, ton, graf] = dobiTon(graf, delta, ton);
  fr = matchFrekvenca(ton, frekvence);
  if fr < 0
    continue;
  endif
  akord = [akord; fr]
  
  akord = akord(akord>=0);
  akord = unique(sortrows(akord));
  
  if size(akord, 1) > 2
    break;
  endif
  
  limit += 1;
endwhile

akord

izhod = "";

if size(akord, 1) < 3
  666
  return;
endif

nota1 = matchIme(akord(1), frekvence, note)
nota2 = matchIme(akord(2), frekvence, note)
nota3 = matchIme(akord(3), frekvence, note)

if strcmpi(nota1, "C1") && strcmpi(nota2, "E1") && strcmpi(nota3, "G1")
  izhod = "Cdur";
elseif strcmpi(nota1, "C1") && strcmpi(nota2, "DIS1") && strcmpi(nota3, "G1")
  izhod = "Cmol";
elseif strcmpi(nota1, "D1") && strcmpi(nota2, "FIS1") && strcmpi(nota3, "A1")
  izhod = "Ddur"
elseif strcmpi(nota1, "D1") && strcmpi(nota2, "F1") && strcmpi(nota3, "A1")
  izhod = "Dmol"  
elseif strcmpi(nota1, "E1") && strcmpi(nota2, "GIS1") && strcmpi(nota3, "H1")
  izhod = "Edur"
elseif strcmpi(nota1, "E1") && strcmpi(nota2, "G1") && strcmpi(nota3, "H1")
  izhod = "Emol"
elseif strcmpi(nota1, "F1") && strcmpi(nota2, "A1") && strcmpi(nota3, "C2")
  izhod = "Fdur"
elseif strcmpi(nota1, "F1") && strcmpi(nota2, "GIS1") && strcmpi(nota3, "C2")
  izhod = "Fmol"
elseif strcmpi(nota1, "G1") && strcmpi(nota2, "H1") && strcmpi(nota3, "D2")
  izhod = "Gdur"
elseif strcmpi(nota1, "G1") && strcmpi(nota2, "B1") && strcmpi(nota3, "D2")
  izhod = "Gmol"
elseif strcmpi(nota1, "A1") && strcmpi(nota2, "CIS2") && strcmpi(nota3, "E2")
  izhod = "Adur"  
elseif strcmpi(nota1, "A1") && strcmpi(nota2, "C2") && strcmpi(nota3, "E2")
  izhod = "Amol"
elseif strcmpi(nota1, "H1") && strcmpi(nota2, "DIS2") && strcmpi(nota3, "FIS2")
  izhod = "Hdur"
elseif strcmpi(nota1, "H1") && strcmpi(nota2, "D2") && strcmpi(nota3, "FIS2")
  izhod = "Hmol"
else
  izhod
endif

endfunction

function [izhod, dolzina, delta] = removeNoise(graf, Fs)
  cela_dolzina = size(graf, 2);
  delta = Fs / cela_dolzina;
  cela_dolzina = cela_dolzina / 2;
  graf = graf(1, 1:cela_dolzina);
  
  while true
    [y, x] = max(graf);
    if x * delta < 1000
      break;
    endif
    
    graf(1, x) = 0;
  endwhile
  
  izhod = graf;
  dolzina = cela_dolzina;
endfunction

function [y, x, noviGraf] = dobiTon(graf, delta, prejsnji)
  while true
    [y, x] = max(graf);
    graf(1, x) = 0;
    
    if abs(x*delta - prejsnji*delta) >= 2
      x = x*delta;
      break;
    endif
  endwhile
  
  noviGraf = graf;
endfunction

function tocnaFrekvenca = matchFrekvenca(frekvenca, frekvence)
  for i = 1:size(frekvence, 2)
    if abs(frekvenca - frekvence(i)) < 2.5
      tocnaFrekvenca = frekvence(i);
      return;
    endif
  endfor
  
  tocnaFrekvenca = -1;
endfunction

function nota = matchIme(frekvenca, frekvence, note)
  for i = 1:size(frekvence, 2)
    if frekvenca == frekvence(i)
      nota = note{i};
      return;
    endif
  endfor
endfunction
