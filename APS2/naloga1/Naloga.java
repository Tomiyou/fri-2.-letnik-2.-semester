import java.util.Scanner;

public class Naloga {
  static int primerjave = 0, prirejanja = 0;

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    Boolean trace = true;
    if (sc.next().equals("count")) {
      trace = false;
    }

    String sort = sc.next();

    Boolean up = true;
    if (sc.next().equals("down")) {
      up = false;
    }

    int len = sc.nextInt();
    int[] seznam = new int[len];
    for (int i = 0; i < len; i++) {
      seznam[i] = sc.nextInt();
    }

    switch (sort) {
    case "bs":
      bubble(trace, up, seznam);
      break;
    case "ss":
      selection(trace, up, seznam);
      break;
    case "is":
      insertion(trace, up, seznam);
      break;
    case "hs":
      heap(trace, up, seznam);
      break;
    case "qs":
      quick(trace, up, seznam);
      break;
    case "ms":
      merge(trace, up, seznam);
      break;
    case "cs":
      count(trace, up, seznam);
      break;
    case "rs":
      selection(trace, up, seznam);
      break;
    default:
      System.out.println("INVALID INPUT SORT: " + sort);
    }

    sc.close();
  }

  // Bubble sort
  public static void bubble(Boolean trace, Boolean up, int[] neurejen) {
    int[] urejen = copySeznam(neurejen);
    bubblesort(trace, up, urejen);
    if (!trace) {
      primerjave = 0;
      prirejanja = 0;
      bubblesort(trace, up, urejen);
      primerjave = 0;
      prirejanja = 0;
      bubblesort(trace, !up, urejen);
    }
  }

  public static void bubblesort(Boolean trace, Boolean up, int[] seznam) {
    int last = seznam.length - 1, neurejen = 0;

    while (neurejen < last) {
      if (trace) {
        printSeznam(neurejen, seznam);
      }

      if (up) {
        for (int i = last; i > neurejen; i--) {
          if (seznam[i] < seznam[i - 1]) {
            swap(i, i - 1, seznam);
          }

          primerjave += 1;
        }
      } else {
        for (int i = last; i > neurejen; i--) {
          if (seznam[i] > seznam[i - 1]) {
            swap(i, i - 1, seznam);
          }

          primerjave += 1;
        }
      }

      neurejen += 1;
    }

    if (trace) {
      printSeznam(neurejen, seznam);
    } else {
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  // Selection sort
  public static void selection(Boolean trace, Boolean up, int[] neurejen) {
    int[] urejen = copySeznam(neurejen);
    selectionsort(trace, up, urejen);
    if (!trace) {
      primerjave = 0;
      prirejanja = 0;
      selectionsort(trace, up, urejen);
      primerjave = 0;
      prirejanja = 0;
      selectionsort(trace, !up, urejen);
    }
  }

  public static void selectionsort(Boolean trace, Boolean up, int[] seznam) {
    int len = seznam.length, neurejen = 0;

    while (neurejen < len - 1) {
      if (trace) {
        printSeznam(neurejen, seznam);
      }

      int minmax = neurejen;
      if (up) {
        // Najdemo najmanjši element v neurejenem delu
        for (int i = neurejen + 1; i < len; i++) {
          if (seznam[i] < seznam[minmax]) {
            minmax = i;
          }
          primerjave += 1;
        }
      } else {
        for (int i = neurejen + 1; i < len; i++) {
          if (seznam[i] > seznam[minmax]) {
            minmax = i;
          }
          primerjave += 1;
        }
      }

      // Zamenjamo prvi neurejen element in najmanjši element v neurejenem delu
      swap(neurejen, minmax, seznam);

      // premaknemo neurejen pointer
      neurejen += 1;
    }

    if (trace) {
      printSeznam(neurejen, seznam);
    } else {
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  // Insertion sort
  public static void insertion(Boolean trace, Boolean up, int[] neurejen) {
    int[] urejen = copySeznam(neurejen);
    insertionsort(trace, up, urejen);
    if (!trace) {
      primerjave = 0;
      prirejanja = 0;
      insertionsort(trace, up, urejen);
      primerjave = 0;
      prirejanja = 0;
      insertionsort(trace, !up, urejen);
    }
  }

  public static void insertionsort(Boolean trace, Boolean up, int[] seznam) {
    int len = seznam.length, neurejen = 1;

    while (neurejen < len) {
      if (trace) {
        printSeznam(neurejen, seznam);
      }

      for (int i = neurejen; i > 0; i--) {
        primerjave += 1;

        if (up) {
          if (seznam[i] < seznam[i - 1]) {
            swap(i, i - 1, seznam);
          } else {
            break;
          }
        } else {
          if (seznam[i] > seznam[i - 1]) {
            swap(i, i - 1, seznam);
          } else {
            break;
          }
        }
      }

      neurejen += 1;
    }

    if (trace) {
      printSeznam(neurejen, seznam);
    } else {
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  // Heap sort
  public static void heap(Boolean trace, Boolean up, int[] neurejen) {
    int[] urejen = copySeznam(neurejen);
    heapsort(trace, up, urejen);
    if (!trace) {
      primerjave = 0;
      prirejanja = 0;
      heapsort(trace, up, urejen);
      primerjave = 0;
      prirejanja = 0;
      heapsort(trace, !up, urejen);
    }
  }

  public static void heapsort(Boolean trace, Boolean up, int[] kopica) {
    int n = kopica.length;

    // naredi kopico
    for (int i = n / 2; i > 0; i--) {
      pogrezni(kopica, i - 1, n, up);
    }

    // uredi kopico
    while (n > 1) {
      if (trace)
        printHeap(kopica, n);
      n -= 1;
      swap(0, n, kopica);
      pogrezni(kopica, 0, n, up);
    }

    if (trace)
      printHeap(kopica, n);
    else
      System.out.println(primerjave + " " + prirejanja);
  }

  static void pogrezni(int a[], int i, int dolzKopice, Boolean descending) {
    int levi_otrok = 2 * i + 1;
    int desni_otrok = 2 * i + 2;

    int zamenjava = -1;
    if (descending) {
      // ali levi otrok obstaja in je vecji od mene
      if (levi_otrok < dolzKopice) {
        primerjave += 1;
        if (a[levi_otrok] > a[i]) {
          zamenjava = levi_otrok;
        }
      }
      // ali desni otrok obstaja in je vecji od mene
      if (desni_otrok < dolzKopice) {
        primerjave += 1;
        if (a[desni_otrok] > a[i] && a[desni_otrok] > a[levi_otrok]) {
          zamenjava = desni_otrok;
        }
      }
    } else {
      // ali levi otrok obstaja in je manjsi od mene
      if (levi_otrok < dolzKopice) {
        primerjave += 1;
        if (a[levi_otrok] < a[i]) {
          zamenjava = levi_otrok;
        }
      }
      // ali desni otrok obstaja in je manjsi od mene
      if (desni_otrok < dolzKopice) {
        primerjave += 1;
        if (a[desni_otrok] < a[i] && a[desni_otrok] < a[levi_otrok]) {
          zamenjava = desni_otrok;
        }
      }
    }

    if (zamenjava != -1) {
      swap(i, zamenjava, a);
      pogrezni(a, zamenjava, dolzKopice, descending);
    }
  }

  static void printHeap(int a[], int n) {
    int level = 1;
    int printedThisLevel = 0;

    for (int i = 0; i < n; i++) {
      if (printedThisLevel++ == level) {
        level *= 2;
        printedThisLevel = 1;
        System.out.printf(" | %d", a[i]);
      } else {
        if (i == 0) {
          System.out.printf("%d", a[i]);
        } else {
          System.out.printf(" %d", a[i]);
        }
      }
    }

    System.out.println();
  }

  // Quick sort
  public static void quick(Boolean trace, Boolean up, int[] seznam) {
    quicksort(seznam, 0, seznam.length - 1, trace, up);

    if (!trace) {
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      quicksort(seznam, 0, seznam.length - 1, trace, up);
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      quicksort(seznam, 0, seznam.length - 1, trace, !up);
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  static void quicksort(int[] seznam, int levi, int desni, Boolean trace, Boolean up) {
    if (levi >= desni)
      return;
    int p = seznam[(levi + desni) / 2];
    prirejanja += 1;
    int i = levi, j = desni;
    while (i <= j) {
      if (up) {
        while (seznam[i] < p) {
          primerjave += 1;
          i += 1;
        }
        while (seznam[j] > p) {
          primerjave += 1;
          j -= 1;
        }
      } else {
        while (seznam[i] > p) {
          primerjave += 1;
          i += 1;
        }
        while (seznam[j] < p) {
          primerjave += 1;
          j -= 1;
        }
      }
      primerjave += 2;
      if (i <= j) {
        swap(i, j, seznam);
        i += 1;
        j -= 1;
      }
    }

    if (trace)
      quicksortPrint(seznam, levi, desni, i, j + 1);

    quicksort(seznam, levi, j, trace, up);
    quicksort(seznam, i, desni, trace, up);
  }

  static void quicksortPrint(int[] seznam, int levi, int desni, int a, int b) {
    for (int i = levi; i <= desni; i++) {
      if (i == a) {
        if (a == b) {
          System.out.print("| | " + seznam[i] + " ");
        } else {
          System.out.print("| " + seznam[i] + " ");
        }
      } else if (i == b) {
        System.out.print("| " + seznam[i] + " ");
      } else {
        System.out.print(seznam[i] + " ");
      }
    }

    System.out.println();
  }

  // Merge sort
  static void merge(Boolean trace, Boolean up, int[] seznam) {
    seznam = mergesort(seznam, 0, seznam.length - 1, trace, up);
    if (!trace) {
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      seznam = mergesort(seznam, 0, seznam.length - 1, trace, up);
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      mergesort(seznam, 0, seznam.length - 1, trace, !up);
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  static int[] mergesort(int[] seznam, int a, int b, Boolean trace, Boolean up) {
    int len = b - a + 1;

    if (len < 2) {
      prirejanja += 1;
      return new int[] { seznam[a] };
    }

    int sredina = (a + b) / 2 + 1;
    if (trace)
      printSeznam(sredina, seznam, a, b);
    int[] n = mergesort(seznam, a, sredina - 1, trace, up);
    int[] m = mergesort(seznam, sredina, b, trace, up);

    int i = 0, j = 0, position = 0;
    int[] rezultat = new int[len];

    while (position < len) {
      prirejanja += 1;
      if (i == n.length) {
        rezultat[position] = m[j];
        j += 1;
      } else if (j == m.length) {
        rezultat[position] = n[i];
        i += 1;
      } else {
        primerjave += 1;
        if (up) {
          if (m[j] < n[i]) {
            rezultat[position] = m[j];
            j += 1;
          } else {
            rezultat[position] = n[i];
            i += 1;
          }
        } else {
          if (m[j] > n[i]) {
            rezultat[position] = m[j];
            j += 1;
          } else {
            rezultat[position] = n[i];
            i += 1;
          }
        }
      }

      position += 1;
    }

    if (trace)
      printSeznam(rezultat);
    return rezultat;

  }

  // Counting sort
  public static void count(Boolean trace, Boolean up, int[] seznam) {
    countingsort(trace, up, seznam);
    if (!trace) {
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      countingsort(trace, up, seznam);
      System.out.println(primerjave + " " + prirejanja);
      primerjave = 0;
      prirejanja = 0;
      countingsort(trace, !up, seznam);
      System.out.println(primerjave + " " + prirejanja);
    }
  }

  public static void countingsort(Boolean trace, Boolean up, int[] seznam) {
    int[] prestete = new int[256];
    for (int i = 0; i < seznam.length; i++) {
      prestete[seznam[i]] += 1;
    }
    int[] pozicije = new int[256];
    int kumulativa = 0;
    for (int i = 0; i < 256; i++) {
      kumulativa += prestete[i];
      pozicije[i] = kumulativa;
      if (trace)
        System.out.print(kumulativa + " ");
    }
    if (trace)
      System.out.println();

    for (int i = seznam.length - 1; i >= 0; i--) {
      pozicije[seznam[i]] -= 1;
      System.out.print(pozicije[seznam[i]] + " ");
    }
    System.out.println();

    for (int i = 0; i < 256; i++) {
      for (int j = 0; j < prestete[i]; j++) {
        if (trace)
          System.out.print(i + " ");
      }
    }
    System.out.println();
  }

  //////////////////////////////////////////////////////////////////////
  /////////////////////////// HELPERS ////////////////////////////////
  //////////////////////////////////////////////////////////////////////
  public static void swap(int a, int b, int[] seznam) {
    int buf = seznam[a];
    seznam[a] = seznam[b];
    seznam[b] = buf;
    prirejanja += 3;
  }

  public static void printSeznam(int[] seznam) {
    for (int i = 0; i < seznam.length; i++) {
      System.out.print(seznam[i] + " ");
    }
    System.out.println();
  }

  public static void printSeznam(int separator, int[] seznam) {
    for (int i = 0; i < seznam.length; i++) {
      if (i == separator) {
        System.out.print("| " + seznam[i] + " ");
      } else {
        System.out.print(seznam[i] + " ");
      }
    }

    if (separator == seznam.length) {
      System.out.println("|");
    } else {
      System.out.println();
    }
  }

  public static void printSeznam(int separator, int[] seznam, int a, int b) {
    for (int i = a; i <= b; i++) {
      if (i == separator) {
        System.out.print("| " + seznam[i] + " ");
      } else {
        System.out.print(seznam[i] + " ");
      }
    }

    if (separator == seznam.length) {
      System.out.println("|");
    } else {
      System.out.println();
    }
  }

  public static int[] copySeznam(int[] seznam) {
    int len = seznam.length;
    int[] out = new int[len];

    for (int i = 0; i < len; i++) {
      out[i] = seznam[i];
    }

    return out;
  }
}