import java.util.Scanner;

public class Izziv {
  static int n, k;
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    n = sc.nextInt();
    k = sc.nextInt();

    int[][] matrika = new int[n+1][k];
    for (int i = 1; i <= n; i++) {
      matrika[i][0] = i;
    }
    for (int i = 0; i < k; i++) {
      matrika[1][i] = 1;
    }

    // zacetek matrika(2, 1)
    int min, tmp;
    for (int j = 1; j < k; j++) {
      for (int i = 2; i <= n; i++) {
        min = Integer.MAX_VALUE;
        // ta loop najde MIN od MAX
        for (int x = 1; x <= i; x++) {
          tmp = Math.max(matrika[x-1][j-1], matrika[i-x][j]);
          if (tmp < min) min = tmp;
        }
        // s(n,k)=1 + min(1≤x≤n)max(s(x−1,k−1),s(n−x,k))
        matrika[i][j] = 1 + min;
      }
    }

    print(matrika);

    sc.close();
  }

  public static void print(int[][] matrika) {
    System.out.print("    ");
    for (int i = 1; i <= k; i++) System.out.printf("%4d", i);
    System.out.println();
    
    for (int i = 0; i <= n; i++) {
      System.out.printf("%4d", i);
      for (int j = 0; j < k; j++) {
        System.out.printf("%4d", matrika[i][j]);
      }
      System.out.println();
    }
  }
}