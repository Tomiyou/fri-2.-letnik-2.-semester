import java.util.*;

public class Izziv {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int w = sc.nextInt(), n = sc.nextInt();

    List<IntPair> stvari = new LinkedList<IntPair>();
    List<IntPair> temp = new LinkedList<IntPair>();
    Comparator<IntPair> compare = new IntPairComparator();
    stvari.add(new IntPair(0, 0));
    print(stvari, 0);

    IntPair current;
    int a, b;
    for (int ff = 0; ff < n; ff++) {
      // add items
      a = sc.nextInt();
      b = sc.nextInt();
      for (Iterator<IntPair> iter = stvari.iterator(); iter.hasNext(); ) {
        current = iter.next();
        temp.add(new IntPair(current.v + a, current.c + b));
      }
      
      // remove unnecessary solutions
      IntPair i = null, j = null;
      for (Iterator<IntPair> tempIter = temp.iterator(); tempIter.hasNext(); ) {
        j = tempIter.next();

        // remove if volume is too big
        if (j.v > w) {
          tempIter.remove();
          continue;
        }

        // remove unnecessary problems
        for (Iterator<IntPair> stvariIter = stvari.iterator(); stvariIter.hasNext(); ) {
          i = stvariIter.next();
          
          if (i.v <= j.v && i.c >= j.c) {
            tempIter.remove();
            break;
          } else if (j.v <= i.v && j.c >= i.c) {
            stvariIter.remove();
          }
        }
      }

      for (Iterator<IntPair> tempIter = temp.iterator(); tempIter.hasNext(); ) {
        j = tempIter.next();
        stvari.add(j);
      }
      
      stvari.sort(compare);
      print(stvari, ff+1);
    }

    sc.close();
  }

  public static void print(List<IntPair> stvari, int i) {
    System.out.print(i + ":");
    for (ListIterator<IntPair> iter = stvari.listIterator(); iter.hasNext(); ) {
      System.out.print(" " + iter.next());
    }
    System.out.println();
  }
}

class IntPair {
  final int v, c;
  IntPair(int v, int c) {
    this.v = v;
    this.c = c;
  }

  @Override
  public String toString() { 
    return String.format("(%d, %d)", this.v, this.c); 
  }
}

class IntPairComparator implements Comparator<IntPair> {
  @Override
  public int compare(IntPair o1, IntPair o2) {
    return o1.v - o2.v;
  }
}