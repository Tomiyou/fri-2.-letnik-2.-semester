import java.util.Scanner;

public class Izziv8 {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int stopnja = sc.nextInt();
    int N = (stopnja - 1) * 2 + 1;
    for (int i = 1; true; i *= 2) {
      if (i >= N) {
        N = i;
        break;
      }
    }
    // System.out.println(N);
    // N = 4;

    Complex w = (new Complex(0, 2 * Math.PI / N)).exp();
    Complex[] polinom1 = new Complex[N];
    Complex[] polinom2 = new Complex[N];

    for (int i = 0; i < N; i++) {
      if (i < stopnja) polinom1[i] = new Complex(sc.nextDouble(), 0);
      else polinom1[i] = new Complex();
    }

    for (int i = 0; i < N; i++) {
      if (i < stopnja) polinom2[i] = new Complex(sc.nextDouble(), 0);
      else polinom2[i] = new Complex();
    }

    polinom1 = vrednostna(polinom1, w);
    polinom2 = vrednostna(polinom2, w);

    for (int i = 0; i < N; i++) {
      polinom1[i] = polinom1[i].times(polinom2[i]);
    }

    Complex[] rezultat = vrednostna(polinom1, w.reciprocal());
    double coef = 1.0 / N;
    for (int i = 0; i < N; i++) {
      rezultat[i] = rezultat[i].times(coef);
    }
    printArray(rezultat);

    sc.close();
  }

  public static Complex[] vrednostna(Complex[] vhodni_polinom, Complex w) {
    // printArray(vhodni_polinom);
    if (vhodni_polinom.length == 1) {
      return vhodni_polinom;
    }

    int polovica_dolzine = vhodni_polinom.length / 2;

    // najprej delimo oba polinoma
    Complex[] sodi_polinom = new Complex[polovica_dolzine];
    for (int i = 0; i < polovica_dolzine; i++) {
      sodi_polinom[i] = vhodni_polinom[i * 2];
    }

    sodi_polinom = vrednostna(sodi_polinom, w.pow(2));

    Complex[] lihi_polinom = new Complex[polovica_dolzine];
    for (int i = 0; i < polovica_dolzine; i++) {
      lihi_polinom[i] = vhodni_polinom[i * 2 + 1];
    }
    lihi_polinom = vrednostna(lihi_polinom, w.pow(2));

    // sedaj ju združimo
    Complex[] izhodni_polinom = new Complex[vhodni_polinom.length];
    for (int i = 0; i < polovica_dolzine; i++) {
      izhodni_polinom[i] = sodi_polinom[i].plus(lihi_polinom[i].times(w.pow(i)));
      izhodni_polinom[i + polovica_dolzine] = sodi_polinom[i].minus(lihi_polinom[i].times(w.pow(i)));
    }
    
    printArray(izhodni_polinom);
    return izhodni_polinom;
  }

  public static void printArray(Complex[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i].toString() + " ");
    }

    System.out.println("");
  }
}

class Complex {
  double re;
  double im;

  public Complex() {
    re = 0;
    im = 0;
  }

  public Complex(double real, double imag) {
    re = real;
    im = imag;
  }

  public String toString() {
    double tRe = (double) Math.round(re * 100000) / 100000;
    double tIm = (double) Math.round(im * 100000) / 100000;
    if (tIm == 0)
      return tRe + "";
    if (tRe == 0)
      return tIm + "i";
    if (tIm < 0)
      return tRe + "-" + (-tIm) + "i";
    return tRe + "+" + tIm + "i";
  }

  public Complex conj() {
    return new Complex(re, -im);
  }

  // sestevanje
  public Complex plus(Complex b) {
    Complex a = this;
    double real = a.re + b.re;
    double imag = a.im + b.im;
    return new Complex(real, imag);
  }

  // odstevanje
  public Complex minus(Complex b) {
    Complex a = this;
    double real = a.re - b.re;
    double imag = a.im - b.im;
    return new Complex(real, imag);
  }

  // mnozenje z drugim kompleksnim stevilo
  public Complex times(Complex b) {
    Complex a = this;
    double real = a.re * b.re - a.im * b.im;
    double imag = a.re * b.im + a.im * b.re;
    return new Complex(real, imag);
  }

  // mnozenje z realnim stevilom
  public Complex times(double alpha) {
    return new Complex(alpha * re, alpha * im);
  }

  // reciprocna vrednost kompleksnega stevila
  public Complex reciprocal() {
    double scale = re * re + im * im;
    return new Complex(re / scale, -im / scale);
  }

  // deljenje
  public Complex divides(Complex b) {
    Complex a = this;
    return a.times(b.reciprocal());
  }

  // e^this
  public Complex exp() {
    return new Complex(Math.exp(re) * Math.cos(im), Math.exp(re) * Math.sin(im));
  }

  // potenca komplesnega stevila
  public Complex pow(int k) {

    Complex c = new Complex(1, 0);
    for (int i = 0; i < k; i++) {
      c = c.times(this);
    }
    return c;
  }

}