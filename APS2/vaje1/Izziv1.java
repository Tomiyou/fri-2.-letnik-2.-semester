import java.util.Arrays;
import java.util.Random;

class Izziv1 {
  public static void main(String[] args) {
    System.out.println("   n     |    linearno  |   dvojisko   |");
    System.out.println("---------+--------------+---------------");

    int korak = 1000;
    for (int i = korak; i <= 100000; i += korak) {
      long linear = timeLinear(i);
      long binary = timeBinary(i);
      System.out.printf("%8s |%13s |%13s %n", i, linear, binary);
    }
  }

  static long timeLinear(int n) {
    int[] seznam = generateTable(n);
    Random random = new Random();

    long startTime = System.nanoTime();
    for (int i = 0; i < 1000; i++) {
      int vrednost = random.nextInt(n) + 1;
      findLinear(seznam, vrednost);
    }
    long executionTime = System.nanoTime() - startTime;

    return executionTime / 1000;
  }

  static long timeBinary(int n) {
    int[] seznam = generateTable(n);
    Random random = new Random();

    long startTime = System.nanoTime();
    for (int i = 0; i < 1000; i++) {
      int vrednost = random.nextInt(n) + 1;
      findBinary(seznam, 0, seznam.length - 1, vrednost);
    }
    long executionTime = System.nanoTime() - startTime;

    return executionTime / 1000;
  }

  static int findLinear(int[] a, int v) {
    for (int i = 0; i < a.length; i++) {
      if (a[i] == v) {
        return i;
      }
    }
    
    return -1;
  }

  static int findBinary(int[] a, int l, int r, int v) {
    while (true) {
      int middle = (r + l) / 2;
      if (a[middle] == v) {
        return middle;
      } else if (a[middle] < v) {
        // vzamemo desno polovico
        l = middle + 1;
      } else {
        // vzamemo levo polovico
        r = middle - 1;
      }
    }
  }

  static int[] generateTable(int n) {
    int[] rezultat = new int[n];

    for (int i = 0; i < n; i++) {
      rezultat[i] = i+1;
    }

    return rezultat;
  }
}