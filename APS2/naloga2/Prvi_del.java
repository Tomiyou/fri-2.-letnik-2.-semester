import java.util.Scanner;

public class Prvi_del {
  static String int2char = "0123456789abcdefghijk";

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    String alg = sc.next();
    int base = sc.nextInt();
    String num1 = sc.next();
    String num2 = sc.next();

    int[] a = new int[num1.length()];
    for (int i = 0; i < a.length; i++) {
      a[a.length - i - 1] = Character.getNumericValue(num1.charAt(i));
    }
    int[] b = new int[num2.length()];
    for (int i = 0; i < b.length; i++) {
      b[b.length - i - 1] = Character.getNumericValue(num2.charAt(i));
    }

    switch (alg) {
    case "os":
      osnovno(base, a, b);
      break;
    case "dv":
      System.out.println(print(delivladaj(base, a, b)));
      break;
      case "ka":
      System.out.println(print(karacub(base, a, b)));
      break;
    }

    sc.close();
  }

  public static void osnovno(int base, int[] a, int[] b) {
    int[] rezultat = new int[0];
    for (int i = 0; i < b.length; i++) {
      int[] stevilo = mnozi(base, a, b[b.length - i - 1]);
      System.out.println(print(stevilo));
      stevilo = offset(stevilo, b.length - 1 - i);
      rezultat = sestej(base, rezultat, stevilo);
    }
    for (int i = 0; i < rezultat.length; i++) {
      System.out.print('-');
    }
    System.out.println();
    System.out.println(print(rezultat));
  }

  public static int[] delivladaj(int base, int[] a, int[] b) {
    System.out.printf("%s %s\n", print(a), print(b));
    if (jeNic(a) || jeNic(b)) return new int[]{0};
    if (a.length == 1) return mnozi(base, b, a[0]);
    if (b.length == 1) return mnozi(base, a, b[0]);

    int n = Math.max(a.length, b.length);
    if (n % 2 != 0) n += 1;
    
    int[] a0 = split(a, n, false);
    int[] a1 = split(a, n, true);
    int[] b0 = split(b, n, false);
    int[] b1 = split(b, n, true);

    int[] a0b0 = delivladaj(base, a0, b0);
    System.out.println(print(a0b0));
    int[] a0b1 = delivladaj(base, a0, b1);
    System.out.println(print(a0b1));
    int[] a1b0 = delivladaj(base, a1, b0);
    System.out.println(print(a1b0));
    int[] a1b1 = delivladaj(base, a1, b1);
    System.out.println(print(a1b1));

    int[] rezultat = new int[0];
    rezultat = sestej(base, rezultat, a0b0);
    rezultat = sestej(base, rezultat, offset(sestej(base, a0b1, a1b0), n/2));
    rezultat = sestej(base, rezultat, offset(a1b1, n));

    return rezultat;
  }

  public static int[] karacub(int base, int[] a, int[] b) {
    System.out.printf("%s %s\n", print(a), print(b));
    if (jeNic(a) || jeNic(b)) return new int[]{0};
    if (a.length == 1) return mnozi(base, b, a[0]);
    if (b.length == 1) return mnozi(base, a, b[0]);

    int n = Math.max(a.length, b.length);
    if (n % 2 != 0) n += 1;
    
    int[] a0 = split(a, n, false);
    int[] a1 = split(a, n, true);
    int[] b0 = split(b, n, false);
    int[] b1 = split(b, n, true);

    int[] a0b0 = karacub(base, a0, b0);
    System.out.println(print(a0b0));
    
    int[] a1b1 = karacub(base, a1, b1);
    System.out.println(print(a1b1));

    int[] produkt = karacub(base, sestej(base, a0, a1), sestej(base, b0, b1));
    System.out.println(print(produkt));
    
    int[] rezultat = new int[0];
    rezultat = sestej(base, rezultat, a0b0);
    produkt = odstej(base, odstej(base, produkt, a1b1), a0b0);
    rezultat = sestej(base, rezultat, offset(produkt, n/2));
    rezultat = sestej(base, rezultat, offset(a1b1, n));

    return rezultat;
  }

  public static int[] split(int[] original, int n, Boolean prefix) {
    int offset = 0, rezultat[];

    if (prefix) {
      rezultat = new int[Math.max(original.length - n/2, 0)];
      offset = n/2;
    } else {
      int nicle = 0;
      for (int i = 0; i < n/2 && i < original.length; i++) {
        if (original[i] != 0) nicle = 0;
        else nicle += 1;
      }
      rezultat = new int[Math.min(n/2 - nicle, original.length)];
    }

    for (int i = 0; i < rezultat.length; i++) rezultat[i] = original[i + offset];
    return rezultat;
  }

  public static int[] mnozi(int base, int[] num1, int num2) {
    int len = num1.length;
    int[] rezultat = num1.clone();
    // gremo cez num2 in mnozimo vsak char z vsakim iz num1
    int tmp, remainder = 0;

    for (int j = 0; j < len; j++) {
      tmp = num2 * num1[j] + remainder;
      rezultat[j] = tmp % base;
      remainder = tmp / base;
    }

    if (remainder != 0) {
      int[] nov_rezultat = new int[len + 1];
      for (int i = 0; i < len; i++) nov_rezultat[i] = rezultat[i];
      nov_rezultat[len] = remainder % base;
      return nov_rezultat;
    }

    return rezultat;
  }

  public static boolean jeNic(int[] stevilo) {
    for (int i = 0; i < stevilo.length; i++) {
      if (stevilo[i] != 0) return false;
    }
    return true;
  }

  public static int[] offset(int[] stevilo, int num) {
    int[] rezultat = new int[stevilo.length + num];
    for (int i = 0; i < stevilo.length; i++) {
      rezultat[i + num] = stevilo[i];
    }
    return rezultat;
  }

  public static int[] sestej(int base, int[] a, int[] b) {
    int[] rezultat;
    if (a.length > b.length) {
      rezultat = a.clone();
      a = b;
    } else
      rezultat = b.clone();

    int tmp, carry = 0, len = rezultat.length, j;

    for (j = 0; j < len; j++) {
      if (j < a.length) tmp = rezultat[j] + a[j] + carry;
      else tmp = rezultat[j] + carry;
      rezultat[j] = tmp % base;
      carry = tmp / base;
    }

    if (carry != 0 && j == len) {
      int[] nov_rezultat = new int[len + 1];
      for (int i = 0; i < len; i++) nov_rezultat[i] = rezultat[i];
      nov_rezultat[len] = carry % base;
      return nov_rezultat;
    }

    return rezultat;
  }

  public static int[] odstej(int base, int[] a, int[] b) {
    int[] rezultat;
    rezultat = a.clone();
    a = b;

    int carry = 0, len = rezultat.length;

    for (int j = 0; j < len; j++) {
      if (j < a.length) rezultat[j] -= a[j] + carry;
      else rezultat[j] -= carry;
      if (rezultat[j] < 0) {
        rezultat[j] += base;
        carry = 1;
      } else
        carry = 0;
    }

    return rezultat;
  }

  public static String print(int[] stevilo) {
    String rezultat = "";
    int printed = 0;

    for (int j = stevilo.length - 1; j >= 0; j--) {
      if (printed == 0 && stevilo[j] == 0) continue;
      else {
        printed += 1;
        rezultat += int2char.charAt(stevilo[j]);
      }
    }

    if (printed == 0) rezultat = "0";
    return rezultat;
  }
}