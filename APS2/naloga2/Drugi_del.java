import java.util.Scanner;

public class Drugi_del {
  static int optional_arg = 1;

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String alg = sc.next();
    if (!alg.equals("os")) optional_arg = sc.nextInt();

    int a_rows = sc.nextInt(), a_cols = sc.nextInt();
    int[][] a_matrika = new int[a_rows][a_cols];
    for (int y = 0; y < a_rows; y++) {
      for (int x = 0; x < a_cols; x++) {
        a_matrika[y][x] = sc.nextInt();
      }
    }

    int b_rows = sc.nextInt(), b_cols = sc.nextInt();
    int[][] b_matrika = new int[b_rows][b_cols];
    for (int y = 0; y < b_rows; y++) {
      for (int x = 0; x < b_cols; x++) {
        b_matrika[y][x] = sc.nextInt();
      }
    }

    int size = 1;
    if (alg.equals("dv") || alg.equals("st")) {
      int max = Math.max(Math.max(a_rows, a_cols), Math.max(b_rows, b_cols));
      while (size < max) {
        size *= 2;
      }
      a_matrika = makeSquare(a_matrika, size);
      b_matrika = makeSquare(b_matrika, size);
    }


    switch (alg) {
    case "os":
      printMatrix(osnovno(a_matrika, b_matrika), a_rows, b_cols);
      break;
    case "bl":
      // printMatrix(a_matrika, a_matrika.length, a_matrika[0].length);
      // System.out.println(Arrays.deepToString(makeBlocks(a_matrika, optional_arg)));
      // printMatrix(b_matrika, b_matrika.length, b_matrika[0].length);
      // System.out.println(Arrays.deepToString(makeBlocks(b_matrika, optional_arg)));
      blocno(makeBlocks(a_matrika, optional_arg), makeBlocks(b_matrika, optional_arg), a_rows, b_cols, optional_arg);
      break;
    case "dv":
      printMatrix(delivladaj(a_matrika, b_matrika), size, size);
      break;
    case "st":
      printMatrix(strassen(a_matrika, b_matrika), size, size);
      break;
    }

    sc.close();
  }
  
  public static int[][] strassen(int[][] a_matrika, int[][] b_matrika) {
    int size = a_matrika.length;
    if (size == optional_arg) return osnovno(a_matrika, b_matrika);

    int[][] a11 = partition(a_matrika, 0, 0);
    int[][] a12 = partition(a_matrika, 1, 0);
    int[][] a21 = partition(a_matrika, 0, 1);
    int[][] a22 = partition(a_matrika, 1, 1);

    int[][] b11 = partition(b_matrika, 0, 0);
    int[][] b12 = partition(b_matrika, 1, 0);
    int[][] b21 = partition(b_matrika, 0, 1);
    int[][] b22 = partition(b_matrika, 1, 1);

    int[][] p1 = printSum(strassen(a11, plusminus(b12, b22, false))),
            p2 = printSum(strassen(plusminus(a11, a12, true), b22)),
            p3 = printSum(strassen(plusminus(a21, a22, true), b11)),
            p4 = printSum(strassen(a22, plusminus(b21, b11, false))),
            p5 = printSum(strassen(plusminus(a11, a22, true), plusminus(b11, b22, true))),
            p6 = printSum(strassen(plusminus(a12, a22, false), plusminus(b21, b22, true))),
            p7 = printSum(strassen(plusminus(a11, a21, false), plusminus(b11, b12, true)));
    
    return compose(
      plusminus(plusminus(plusminus(p5, p4, true), p2, false), p6, true),
      plusminus(p1, p2, true),
      plusminus(p3, p4, true),
      plusminus(plusminus(plusminus(p1, p5, true), p3, false), p7, false)
    );
  }

  public static int[][] delivladaj(int[][] a_matrika, int[][] b_matrika) {
    int size = a_matrika.length;
    if (size == optional_arg) return osnovno(a_matrika, b_matrika);

    int[][] a11 = partition(a_matrika, 0, 0);
    int[][] a12 = partition(a_matrika, 1, 0);
    int[][] a21 = partition(a_matrika, 0, 1);
    int[][] a22 = partition(a_matrika, 1, 1);

    int[][] b11 = partition(b_matrika, 0, 0);
    int[][] b12 = partition(b_matrika, 1, 0);
    int[][] b21 = partition(b_matrika, 0, 1);
    int[][] b22 = partition(b_matrika, 1, 1);

    return compose(
      plusminus(printSum(delivladaj(a11, b11)), printSum(delivladaj(a12, b21)), true),
      plusminus(printSum(delivladaj(a11, b12)), printSum(delivladaj(a12, b22)), true),
      plusminus(printSum(delivladaj(a21, b11)), printSum(delivladaj(a22, b21)), true),
      plusminus(printSum(delivladaj(a21, b12)), printSum(delivladaj(a22, b22)), true)
    );
  }

  public static void blocno(int[][][][] A, int[][][][] B, int rows, int cols, int blockSize) {
    int[][] rezultat = new int[A.length * blockSize][B[0].length * blockSize];

    for (int i = 0; i < A.length; i++) {
      for (int j = 0; j < B[0].length; j++) {
        for (int k = 0; k < A[i].length; k++) {
          int[][] produkt = printSum(osnovno(A[i][k], B[k][j]));

          for (int y = 0; y < blockSize; y++) {
            for (int x = 0; x < blockSize; x++) {
              rezultat[i*blockSize + y][j*blockSize + x] += produkt[y][x];
            }
          }
        }
      }
    }
    
    printMatrix(rezultat, rows, cols);
  }

  public static int[][] osnovno(int[][] a_matrika, int[][] b_matrika) {
    int a_rows = a_matrika.length, a_cols = a_matrika[0].length, b_cols = b_matrika[0].length;
    int[][] rezultat = new int[a_rows][b_cols];

    int vsota;
    for (int i = 0; i < a_rows; i++) {
      for (int k = 0; k < b_cols; k++) {
        vsota = 0;

        for (int j = 0; j < a_cols; j++) {
          vsota += a_matrika[i][j] * b_matrika[j][k];
        }

        rezultat[i][k] = vsota;
      }
    }
    
    return rezultat;
  }

  public static int[][] plusminus(int[][] a_matrika, int[][] b_matrika, boolean plus) {
    int[][] rezultat = new int[a_matrika.length][a_matrika.length];

    for (int i = 0; i < rezultat.length; i++) {
      for (int j = 0; j < rezultat.length; j++) {
        if (plus) rezultat[i][j] = a_matrika[i][j] + b_matrika[i][j];
        else rezultat[i][j] = a_matrika[i][j] - b_matrika[i][j];
      }
    }
    
    return rezultat;
  }

  public static int[][] partition(int[][] matrika, int offset_x, int offset_y) {
    int n = matrika.length / 2;
    offset_x *= n;
    offset_y *= n;
    int[][] rezultat = new int[n][n];

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        rezultat[i][j] = matrika[offset_y + i][offset_x + j];
      }
    }

    return rezultat;
  }

  public static int[][] compose(int[][] a11, int[][] a12, int[][] a21, int[][] a22) {
    int part_size = a11.length;
    int[][] rezultat = new int[part_size*2][part_size*2];

    for (int i = 0; i < part_size; i++) {
      for (int j = 0; j < part_size; j++) {
        // rezultat[i][j] = matrika[offset_y + i][offset_x + j];
        rezultat[i][j] = a11[i][j];
        rezultat[i][j + part_size] = a12[i][j];
        rezultat[i + part_size][j] = a21[i][j];
        rezultat[i + part_size][j + part_size] = a22[i][j];
      }
    }

    return rezultat;
  }

  public static int[][] makeSquare(int[][] matrika, int size) {
    int[][] rezultat = new int[size][size];

    for (int i = 0; i < matrika.length; i++) {
      for (int j = 0; j < matrika[0].length; j++) {
        rezultat[i][j] = matrika[i][j];
      }
    }

    return rezultat;
  }

  public static int[][][][] makeBlocks(int[][] matrika, int blockSize) {
    int vrstice = matrika.length/blockSize, stolpci = matrika[0].length/blockSize;
    if (matrika.length % blockSize != 0) vrstice++;
    if (matrika[0].length % blockSize != 0) stolpci++;

    int[][][][] rezultat = new int[vrstice][stolpci][blockSize][blockSize];

    for (int i = 0; i < matrika.length; i++) {
      for (int j = 0; j < matrika[0].length; j++) {
        rezultat[i/blockSize][j/blockSize][i%blockSize][j%blockSize] = matrika[i][j];
      }
    }

    return rezultat;
  }

  public static void printMatrix(int[][] matrika, int rows, int cols) {
    System.out.printf("DIMS: %dx%d\n", rows, cols);
    for (int y = 0; y < rows; y++) {
      for (int x = 0; x < cols; x++) {
        System.out.print(matrika[y][x] + " ");
      }
      System.out.println();
    }

  }

  public static int[][] printSum(int[][] matrika) {
    int vsota = 0;
    for (int y = 0; y < matrika.length; y++) {
      for (int x = 0; x < matrika[0].length; x++) {
        vsota += matrika[y][x];
      }
    }

    System.out.println(vsota);
    return matrika;
  }
}