import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Izziv {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();

    int prime = n;
    while (true) {
      if (jePrastevilo(prime) && najdi(n, prime)) {
        break;
      }

      prime += 1;
    }

    sc.close();
  }

  public static boolean najdi(int n, int prime) {
    List<Integer> koreni = new ArrayList<Integer>();

    for (int i = 1; i < prime; i++) {
      for (int j = 1; j <= n; j++) {
        if (pow(i, j) % prime == 1) {
          if (j != n) {
            break;
          } else {
            koreni.add(i);
          }
        }
      }
    }

    if (koreni.isEmpty())
      return false;

    System.out.print(prime + ":");
    Iterator iter = koreni.iterator();
    while (iter.hasNext()) {
      System.out.print(" " + iter.next());
    }
    System.out.println();

    int w = koreni.get(0);
    int[][] f_matrix = new int[n][n];
    for (int j = 0; j < n; j++) {
      f_matrix[j][0] = 1;
      f_matrix[0][j] = 1;
    }

    for (int i = 1; i < n; i++) {
      for (int j = 1; j < n; j++) {
        f_matrix[i][j] = (int) (pow(w, i * j) % prime);
      }
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf(" %d ", f_matrix[i][j]);
      }
      System.out.println();
    }

    return true;
  }

  public static long pow(int a, int b) {
    long rezultat = 1;
    while (b > 0) {
      rezultat *= a;
      b -= 1;
    }

    return rezultat;
  }

  public static boolean jePrastevilo(int n) {
    if (n % 2 == 0)
      return false;

    for (int i = 3; i * i <= n; i += 2) {
      if (n % i == 0) {
        return false;
      }
    }

    return true;
  }
}