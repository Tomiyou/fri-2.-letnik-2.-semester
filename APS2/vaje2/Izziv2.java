import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 

class Izziv2 {
  public static void main(String[] args) throws IOException  {
  //Enter data using BufferReader 
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
      
    // Reading data using readLine 
    int n = Integer.parseInt(reader.readLine());
    String[] values = reader.readLine().split(" ");

    int[] kopica = new int[n];
    for (int i = 0; i < values.length; i++) {
      kopica[i] = Integer.parseInt(values[i]);
    }

    // naredi kopico
    for (int i = n/2; i > 0; i--) {
      pogrezni(kopica, i-1, n);
    }

    izpisi(kopica, n);
    
    // uredi kopico
    while (n > 1) {
      n -= 1;
      zamenjaj(kopica, 0, n);
      pogrezni(kopica, 0, n);
      izpisi(kopica, n);
    }
  }

  static void pogrezni(int a[], int i, int dolzKopice) {
    int levi_otrok = 2 * i + 1;
    int desni_otrok = 2 * i + 2;

    int zamenjava = -1;
    // ali levi otrok obstaja in je vecji od mene
    if (levi_otrok < dolzKopice && a[levi_otrok] > a[i]) {
      zamenjava = levi_otrok;      
    }
    // ali desni otrok obstaja in je vecji od mene
    if (desni_otrok < dolzKopice && a[desni_otrok] > a[i] && a[desni_otrok] > a[levi_otrok]) {
      zamenjava = desni_otrok;
    }

    if (zamenjava != -1) {
      zamenjaj(a, i, zamenjava);
      pogrezni(a, zamenjava, dolzKopice);
    } else {
      return;
    }
  }

  static void izpisi(int a[], int n) {
    int level = 1;
    int printedThisLevel = 0;

    for (int i = 0; i < n; i++) {
      if (printedThisLevel++ == level) {
        level *= 2;
        printedThisLevel = 1;
        System.out.printf(" | %d", a[i]);
      } else {
        if (i == 0) {
          System.out.printf("%d", a[i]);
        } else {
          System.out.printf(" %d", a[i]);
        }
      }
    }

    System.out.println();
  }

  static void zamenjaj(int seznam[], int a, int b) {
    int tmp = seznam[a];
    seznam[a] = seznam[b];
    seznam[b] = tmp;
  }
}