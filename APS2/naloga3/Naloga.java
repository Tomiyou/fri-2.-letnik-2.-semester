import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.LinkedList;

public class Naloga {
  public static LinkedList<StringBuilder> output;
  public static int lines;

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    output = new LinkedList<StringBuilder>();
    output.add(new StringBuilder());
    lines = 0;

    String[] prva = sc.nextLine().split(" ");
    int lineLimit = 0;
    if (prva.length > 1) {
      lineLimit = Integer.parseInt(prva[1]);
    }
    int n = sc.nextInt(), m = sc.nextInt();

    NeusmerjenGraf graf = new NeusmerjenGraf(n);

    // naredimo povezave
    for (int i = 0; i < m; i++) {
      graf.dodajPovezavo(sc.nextInt(), sc.nextInt());
    }

    switch (prva[0]) {
    case "2c":
      if (dvebarve(graf)) {
        System.out.println("OK");
      } else {
        System.out.println("NOK");
      }
      break;
    case "gr":
      pozresno(graf);
      break;
    case "ex":
      exhausting(graf);
      break;
    case "bt":
      sestopanje(graf);
      break;
    case "dp":
      break;
    }

    Iterator<StringBuilder> iter = output.iterator();
    int skip = lines - lineLimit;
    if (lineLimit != 0) {
      while (iter.hasNext() && skip > 0) {
        iter.next();
        --skip;
      }
    }
  
    while (iter.hasNext()) {
      System.out.println(iter.next());
    }

    sc.close();
  }

  public static boolean dvebarve(NeusmerjenGraf graf) {
    int barva = 0;
    Set<Vozlisce> iteracija = new TreeSet<Vozlisce>(new VozlisceCompare());
    iteracija.add(graf.nodes[0]);
    while (!iteracija.isEmpty()) {
      print2b(iteracija, barva);
      Set<Vozlisce> buf = new TreeSet<Vozlisce>(new VozlisceCompare());

      // gremo čez vsa vozlišča v iteraciji
      Iterator<Vozlisce> nkorak = iteracija.iterator();
      while (nkorak.hasNext()) {
        Vozlisce current = nkorak.next();
        current.barva = barva % 2;

        Iterator<Vozlisce> sosedje = current.sosedi.iterator();
        // gremo čez vse sosede trenutnega vozlišča
        while (sosedje.hasNext()) {
          Vozlisce sosed = sosedje.next();
          if (sosed.barva == -1) {
            // sosed še ni bil obiskan, dodamo v kopico
            buf.add(sosed);
          } else if (sosed.barva == current.barva) {
            // System.out.printf("Napaka %d %d\n", current.id, sosed.id);
            return false;
          }
        }
      }

      nkorak = buf.iterator();

      iteracija = buf;
      ++barva;
    }

    return true;
  }

  public static void print2b(Set<Vozlisce> buf, int i) {
    System.out.print(i + " :");
    Iterator<Vozlisce> iter = buf.iterator();
    while (iter.hasNext()) {
      System.out.print(" " + iter.next().id);
    }
    System.out.println();
  }

  public static void pozresno(NeusmerjenGraf graf) {
    int nodeCount = graf.nodes.length;
    boolean[] zasedeneBarve = new boolean[nodeCount];
    graf.nodes[0].barva = 0;

    // gremo čez vsak node in ji damo barvo
    for (int i = 0; i < nodeCount; i++) {
      for (int j = 0; j < nodeCount; j++) {
        zasedeneBarve[j] = false;
      }

      // gremo čez vse sosede
      for (Iterator<Vozlisce> iter = graf.nodes[i].sosedi.iterator(); iter.hasNext();) {
        Vozlisce sosed = iter.next();
        if (sosed.barva != -1)
          zasedeneBarve[sosed.barva] = true;
      }

      for (int j = 0; j < nodeCount; j++) {
        if (zasedeneBarve[j] == false) {
          graf.nodes[i].barva = j;
          break;
        }
      }

      System.out.printf("%d : %d\n", i, graf.nodes[i].barva);
    }
  }

  public static void exhausting(NeusmerjenGraf graf) {
    int k = 2, stVozlisc = graf.nodes.length;
    int[] barve = new int[stVozlisc];
    while (true) {
      // System.out.println("k = " + k);

      for (int x = (int) Math.pow(k, stVozlisc);;) {
        for (int i = 0; i < stVozlisc; i++) {
          graf.nodes[i].barva = barve[i];
          print(barve[i] + " ");
        }

        // gremo čez vsako vozlišče in njegove sosede
        boolean err = false;
        for (int i = 0; i < stVozlisc && err == false; i++) {
          if (veljavnaBarva(graf.nodes[i]) == false) {
            printLINE("NOK");
            err = true;
            break;
          }
        }
        if (err == false) {
          printLINE("OK");
          return;
        }

        x -= 1;
        if (x == 0)
          break;

        barve[stVozlisc - 1] += 1;
        if (barve[stVozlisc - 1] == k) {
          for (int j = stVozlisc - 1; barve[j] == k; j--) {
            barve[j] = 0;
            barve[j - 1] += 1;
          }
        }
      }

      for (int i = 0; i < stVozlisc; i++) {
        barve[i] = 0;
      }
      ++k;
    }
  }

  public static void sestopanje(NeusmerjenGraf graf) {
    int[] barve = new int[graf.nodes.length];
    int k = 2;

    while (true) {
      printLINE("k = " + k);
      if (recurs(graf, barve, 0, k)) {
        break;
      }

      ++k;
    }
  }

  public static boolean recurs(NeusmerjenGraf graf, int[] barve, int i, int k) {
    Vozlisce[] vozlisca = graf.nodes;
    if (i == vozlisca.length) return true;
    
    int max = Math.min(k - 1, i);
    for (int fff = 0; fff <= max; fff++) {
      vozlisca[i].barva = fff;
      barve[i] = fff;

      for (int x = 0; x <= i; x++) {
        print(barve[x] + " ");
      }

      if (veljavnaBarva(vozlisca[i])) {
        printLINE("OK");
        if (recurs(graf, barve, i+1, k))
          return true;

        graf.ponastaviVozlisca(i+1);
      } else {
        printLINE("NOK");
      }
    }

    return false;
  }

  public static boolean veljavnaBarva(Vozlisce jaz) {
    // gremo čez vse sosede
    for (Iterator<Vozlisce> iter = jaz.sosedi.iterator(); iter.hasNext();) {
      Vozlisce sosed = iter.next();
      if (sosed.barva == jaz.barva)
        return false;
    }

    return true;
  }

  public static void print(String str) {
    output.getLast().append(str);
  }

  public static void printLINE(String str) {
    output.getLast().append(str);
    output.add(new StringBuilder());
    lines += 1;
  }
}

class NeusmerjenGraf {
  int stevilo_vozlisc;
  Vozlisce[] nodes;

  public NeusmerjenGraf(int n) {
    nodes = new Vozlisce[n];

    for (int i = 0; i < n; i++) {
      nodes[i] = new Vozlisce(i);
    }
  }

  public void dodajPovezavo(int a, int b) {
    nodes[a].sosedi.add(nodes[b]);
    nodes[b].sosedi.add(nodes[a]);
  }

  public void ponastaviVozlisca() {
    for (int i = 0; i < nodes.length; i++) {
      nodes[i].barva = -1;
      nodes[i].marked = false;
    }
  }

  public void ponastaviVozlisca(int start) {
    for (int i = start; i < nodes.length; i++) {
      nodes[i].barva = -1;
      nodes[i].marked = false;
    }
  }
}

class Vozlisce {
  boolean marked;
  int barva, id;
  ArrayList<Vozlisce> sosedi;

  public Vozlisce(int id) {
    marked = false;
    sosedi = new ArrayList<Vozlisce>();
    barva = -1;
    this.id = id;
  }
}

class VozlisceCompare implements Comparator<Vozlisce> {
  @Override
  public int compare(Vozlisce v1, Vozlisce v2) {
    return v1.id - v2.id;
  }
}