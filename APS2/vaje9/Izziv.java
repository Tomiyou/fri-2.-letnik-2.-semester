import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

public class Izziv {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int stevilo_vozlisc = sc.nextInt();
    Network graf = new Network(stevilo_vozlisc);

    while (sc.hasNext()) {
      graf.addConnection(sc.nextInt(), sc.nextInt(), sc.nextInt());
    }

    while (najdiPot(graf)) {
      Node vozlisce = graf.nodes[graf.nodes.length-1];
      int finalFlow = vozlisce.incFlow;
      System.out.print(finalFlow + ": ");
      
      while (vozlisce.augmEdge != null) {
        System.out.print(vozlisce.id);

        if (vozlisce.positive) {
          System.out.print("+  ");
          vozlisce.augmEdge.currFlow += finalFlow;
          vozlisce = graf.nodes[vozlisce.augmEdge.startID];
        } else {
          System.out.print("-  ");
          vozlisce.augmEdge.currFlow -= finalFlow;
          vozlisce = graf.nodes[vozlisce.augmEdge.endID];
        }
      }
      System.out.println("0");

      // print(graf);
      
      graf.resetMarks();
    }

    sc.close();
  }

  public static boolean najdiPot(Network graf) {
    Node trenutnoVozlisce, sosed;
    graf.nodes[0].marked = true;
    graf.nodes[0].incFlow = Integer.MAX_VALUE;

    while ((trenutnoVozlisce = najdiProstoVozlisce(graf)) != null) {
      // System.out.println("ID: " + trenutnoVozlisce.id);
      // if (trenutnoVozlisce == graf.nodes[graf.nodes.length-1]) return true;
      trenutnoVozlisce.visited = true;

      Iterator<Edge> i = trenutnoVozlisce.outEdges.iterator();
      Edge edge;
      while (i.hasNext()) {
        edge = i.next();
        sosed = graf.nodes[edge.endID];

        if (!sosed.marked && edge.currFlow < edge.capacity) {
          sosed.marked = true;
          sosed.augmEdge = edge;
          sosed.positive = true;
          sosed.incFlow = Math.min(trenutnoVozlisce.incFlow, edge.capacity - edge.currFlow);
        }
      }
      
      i = trenutnoVozlisce.inEdges.iterator();
      while (i.hasNext()) {
        edge = i.next();
        sosed = graf.nodes[edge.startID];
        
        if (!sosed.marked && edge.currFlow > 0) {
          sosed.marked = true;
          sosed.augmEdge = edge;
          sosed.positive = false;
          sosed.incFlow = Math.max(trenutnoVozlisce.incFlow, edge.currFlow);
        }
      }

      if (graf.nodes[graf.nodes.length-1].marked) {
        return true;
      }
    }
    
    return false;
  }

  public static Node najdiProstoVozlisce(Network graf) {
    for (int i = 0; i < graf.nodes.length; i++) {
      if (!graf.nodes[i].visited &&graf.nodes[i].marked) return graf.nodes[i];
    }
    
    return null;
  }
  public static Node print(Network graf) {
    for (int k = 0; k < graf.nodes.length; k++) {
      Iterator<Edge> i = graf.nodes[k].outEdges.iterator();
      Edge edge;
      while (i.hasNext()) {
        edge = i.next();

        System.out.printf("%d --  %d  -> %d\n", edge.startID, edge.currFlow, edge.endID);
      }
    }
    
    return null;
  }
}

class Node{	
	int id;
	//marks for the algorithm
	//------------------------------------
	boolean marked = false, visited = false, positive;
	Edge augmEdge = null; //the edge over which we brought the flow increase
  int incFlow = -1; //-1 means a potentially infinite flow
	//------------------------------------
	ArrayList<Edge> inEdges;
	ArrayList<Edge> outEdges;
	
	public Node(int i) {
		id = i;
		inEdges = new ArrayList<Edge>();
		outEdges = new ArrayList<Edge>();
	}
}

class Edge{
	int startID; 
	int endID;
	int capacity; 
	int currFlow;
	
	public Edge(int fromNode, int toNode, int capacity2) {
		startID = fromNode;
		endID = toNode;
		capacity = capacity2;
		currFlow = 0;
	}
}

class Network{
	Node[] nodes;
	
	/**
	 * Create a new network with n nodes (0..n-1).
	 * @param n the size of the network.
	 */
	public Network(int n){
		nodes = new Node[n];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i]= new Node(i);
		}
	}
	/**
	 * Add a connection to the network, with all the corresponding in and out edges.
	 * @param fromNode
	 * @param toNode
	 * @param capacity
	 */
	public void addConnection(int fromNode, int toNode, int capacity){
		Edge e = new Edge(fromNode, toNode, capacity);
		nodes[fromNode].outEdges.add(e);
		nodes[toNode].inEdges.add(e);
	}

	/**
	 * Reset all the marks of the algorithm, before the start of a new iteration.
	 */
	public void resetMarks() {
		for (int i = 0; i < nodes.length; i++) {
			nodes[i].marked = false;
			nodes[i].visited = false;
			nodes[i].augmEdge = null;
			nodes[i].incFlow = -1;
		}
	}
}