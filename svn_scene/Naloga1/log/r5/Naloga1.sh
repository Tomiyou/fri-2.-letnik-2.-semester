function izpisiPomoc {
  echo "Uporaba: $0 akcija parametri"  
}

function gcd {
  if (( $2 == 0 )); then
    return $1
  else
    gcd $2 $(($1%$2))
    return $?
  fi
}

function jePrestopno {
  local prestopno=false

  # ali je deljivo s 4
  if (( $1%4 == 0)); then
    prestopno=true
  fi

  # ali je deljivo s 100
  if (( $1%100 == 0)); then
    prestopno=false
  fi

  # ali je deljivo s 400
  if (( $1%400 == 0)); then
    prestopno=true
  fi

  if $prestopno; then
    echo "Leto $1 je prestopno."
  else
    echo "Leto $1 ni prestopno."
  fi
}

function fibonnaci {
  local a=0
  local b=1
  local tmp=0

  for (( i=$1; i > 0; i-- )); do
    tmp=$a
    a=$b
    b=$(( $tmp + $b ))
  done

  echo "$1: $a"
}

function userInfo {
  echo -n "$1: "

  # grep line in /etc/passwd
  local info=$(cat /etc/passwd | grep $1)
  # info="tomi:x:1000:1000:Tomaž Hribernik,,,:/home/tomi:/bin/bash"

  # check if user exists (if grep actually returned something)
  if [[ $info == "" ]]; then
    echo "err"
    return
  fi

  # check uid and gid
  echo $info | awk -F: '$3 == $4 { printf "enaka " }'

  # check home directory
  echo "$info" | grep "/home/" > /dev/null && echo -n "obstaja "

  # print number of groups the user is in
  cat /etc/group | grep $1 -c
}

function izpisiTocke {
  # najprej preverimo ali je to komentar oz. prazna vrstica
  [[ $1 == "#" || $1 == "" ]] && return 1
  
  # sestejemo argumente
  local vsota=$(( $2 + $3 + $4 ))

  # preverimo pogoje
  if [[ $5 == "p" || $5 == "P" ]]; then
    vsota=$(( $vsota / 2 ))
  elif [[ ${1:2:2} == "14" ]]; then
    vsota=$(( $vsota + ( RANDOM % 5 ) + 1 ))
  fi

  # vsota ne sme biti vecja od 50
  if (( $vsota > 50 )); then
    vsota=50
  fi

  povprecneTocke=$(( $povprecneTocke + $vsota ))
  echo "$1: $vsota"
  return 0
}

function printFileType {
  printf '%-5s ' "$1"
}

# 1 : max globina, 2 : trenutna globina
function walkPrint {
  [[ $1 < $2 ]] && return

  for n in *; do
    # match file, also checks if it actually exists
    # FILE, DIR, LINK, CHAR, BLOCK, PIPE, SOCK
    local fileType=""
    if [ -S "$n" ]; then fileType="SOCK";
    elif [ -p "$n" ]; then fileType="PIPE";
    elif [ -b "$n" ]; then fileType="BLOCK";
    elif [ -c "$n" ]; then fileType="CHAR";
    elif [ -L "$n" ]; then fileType="LINK";
    elif [ -d "$n" ]; then fileType="DIR";
    elif [ -f "$n" ]; then fileType="FILE";
    else continue
    fi

    # print ----
    for (( i=0; i < $2; i++ )); do
      echo -n "----"
    done

    # ce je file directory je potrebno ubrati drugacno pot
    printFileType $fileType
    if [[ $fileType == "DIR" ]]; then
      echo "$n $2"

      # preden gremo v rekurzijo shranimo pwd, se premaknemo v ustrezno mapo, ter povecamo globino
      local oldpwd=$PWD
      cd $n
      walkPrint $1 $(($2 + 1))

      # po rekurziji gremo nazaj v trenutno mapo
      cd $oldpwd
    else
      echo "$n"
    fi
  done

  return 0
}

function walkSize {
  local info=""
  for n in *; do
    if [ -e "$n" ]; then
      # info=$(stat --printf="%s" "$n")
      # stat -t "$n"
      stat --format="%n %s %b %f %u %g %D %i %h %t %T %F"
    fi
    if [ -d "$n" ]; then
      # preden gremo v rekurzijo shranimo pwd, se premaknemo v ustrezno mapo, ter povecamo globino
      local oldpwd=$PWD
      cd $n
      echo $PWD
      walkSize

      # po rekurziji gremo nazaj v trenutno mapo
      cd $oldpwd
      echo $PWD
    # elif [ -e "$n" ]; then
      # (( stBajtov = $stBajtov + $(stat --printf="%s" "$n") ))
      # echo "$n: $stBajtov"
    fi
  done
}

# shranimo ukaz, ter premaknemo argumente za nadaljno uporabo
ukaz=$1
shift

# nastavimo seed za nakljucna stevila
RANDOM=42

# izvedemo podan ukaz
case $ukaz in
  (pomoc)
    izpisiPomoc
    ;;

  (status)
    gcd $1 $2
    exit $?
    ;;

  (leto)
    for letnica in "$@" ; do
      jePrestopno $letnica
    done
    ;;

  (fib)
    for stevilo in "$@" ; do
      fibonnaci $stevilo
    done
    ;;

  (userinfo)
    cat /etc/passwd
    for user in "$@" ; do
      userInfo $user
    done
    ;;

  (tocke)
    KONEC=false
    stStudentov=0
    povprecneTocke=0

    until $KONEC; do
      read vpisna a b c polovica || KONEC=true
      if izpisiTocke $vpisna $a $b $c $polovica; then
        stStudentov=$(( $stStudentov + 1 ))
      fi
    done

    povprecneTocke=$(( $povprecneTocke / $stStudentov ))
    echo "St. studentov: $stStudentov"
    echo "Povprecne tocke: $povprecneTocke"
    ;;

  (drevo)
    ls -lR
    globina=3
    [[ $1 != "" ]] && cd $1
    [[ $2 != "" ]] && globina=$2

    printFileType "DIR"
    echo ${PWD##*/}
    walkPrint $globina 1
    ;;
  (prostor)
    [[ $1 != "" ]] && cd $1
    stBajtov=0
    stBlokov=0
    zasedenProstor=0
    walkSize

    echo "Velikost: $stBajtov"
    echo "Blokov: $stBlokov"
    echo "Prostor: $zasedenProstor"
    ;;
  (*)
    echo "Napacna uporaba skripte!"
    izpisiPomoc
    ;;
esac