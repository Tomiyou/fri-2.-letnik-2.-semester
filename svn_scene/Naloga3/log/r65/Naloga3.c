#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/wait.h>

#define MAX_TOKENS 30
#define LINE_LENGTH 512

// function declarations
int tokenize(char *line, char **target);
int list_files(char *path, int inode);
int pipeline();

int name_cmd();
int help_cmd();
int status_cmd();
int exit_cmd();
int print_cmd();
int echo_cmd();
int pid_cmd();
int ppid_cmd();

int dirchange_cmd();
int dirwhere_cmd();
int dirmake_cmd();
int dirremove_cmd();
int dirlist_cmd();

int linkhard_cmd();
int linksoft_cmd();
int linkread_cmd();
int linklist_cmd();
int unlink_cmd();
int rename_cmd();
int cpcat_cmd();

// global variables
char *tokens[MAX_TOKENS],
  line[LINE_LENGTH],
  shell_name[64];
bool interactive = false,
  detach = false;
int token_count, last_exit_status = 0,
  redirect_in = -1, redirect_out = -1;

int main(int argc, char **argv) {
  interactive = isatty(0);

  // set default shell_name
  memcpy(shell_name, "mysh", 4);
  shell_name[4] = '\0';

  while (true) {
    // print the name of the shell if in interactive mode
    if (interactive) printf("%s>", shell_name);

    // read line and parse it
    if (fgets(line, LINE_LENGTH, stdin) == NULL) break;
    printf("Vrstica: %s.\n", line);
    token_count = tokenize(line, tokens);

    // check for empty line or comment
    if (token_count == 0 || tokens[0][0] == '#') continue;

    for (int i = 0; i < token_count; i++) {
      printf("Arg: %s.\n", tokens[i]);
    }
    continue;

    // check for detach
    if (strcmp(tokens[token_count-1], "&") == 0) {
      --token_count;

      int pid = fork();
      if (pid == 0) {
        detach = true;
      } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);
        continue;
      } else {
        perror("Fork error");
        exit(-1);
      }
    }

    // check for redirect output
    if (tokens[token_count-1][0] == '>') {
      // save old stdout
      redirect_out = dup(1);
      --token_count;

      char* file = tokens[token_count] + 1;
      int output = open(file, O_WRONLY | O_CREAT, 0644);
      if (output == -1) {
        perror(file);
        continue;
      }

      dup2(output, 1);
      close(output);
    }
    // check for redirect input
    if (tokens[token_count-1][0] == '<') {
      // save old stdin
      redirect_in = dup(0);
      --token_count;

      char* file = tokens[token_count] + 1;
      int input = open(file, O_RDONLY);
      if (input == -1) {
        perror(file);
        continue;
      }

      dup2(input, 0);
      close(input);
    }

    // run the correct function
    int return_val = 666;
    if (strcmp(tokens[0], "name") == 0) return_val = name_cmd();
    else if (strcmp(tokens[0], "help") == 0) return_val = help_cmd();
    else if (strcmp(tokens[0], "status") == 0) return_val = status_cmd();
    else if (strcmp(tokens[0], "exit") == 0) return_val = exit_cmd();
    else if (strcmp(tokens[0], "print") == 0) return_val = print_cmd();
    else if (strcmp(tokens[0], "echo") == 0) return_val = echo_cmd();
    else if (strcmp(tokens[0], "pid") == 0) return_val = pid_cmd();
    else if (strcmp(tokens[0], "ppid") == 0) return_val = ppid_cmd();

    else if (strcmp(tokens[0], "dirchange") == 0) return_val = dirchange_cmd();
    else if (strcmp(tokens[0], "dirwhere") == 0) return_val = dirwhere_cmd();
    else if (strcmp(tokens[0], "dirmake") == 0) return_val = dirmake_cmd();
    else if (strcmp(tokens[0], "dirremove") == 0) return_val = dirremove_cmd();
    else if (strcmp(tokens[0], "dirlist") == 0) return_val = dirlist_cmd();

    else if (strcmp(tokens[0], "linkhard") == 0) return_val = linkhard_cmd();
    else if (strcmp(tokens[0], "linksoft") == 0) return_val = linksoft_cmd();
    else if (strcmp(tokens[0], "linkread") == 0) return_val = linkread_cmd();
    else if (strcmp(tokens[0], "linklist") == 0) return_val = linklist_cmd();
    else if (strcmp(tokens[0], "unlink") == 0) return_val = unlink_cmd();
    else if (strcmp(tokens[0], "rename") == 0) return_val = rename_cmd();
    else if (strcmp(tokens[0], "cpcat") == 0) return_val = cpcat_cmd();

    else if (strcmp(tokens[0], "pipes") == 0) return_val = pipeline();

    else {
      // external commad, forking and execing
      int pid = fork();
      if (pid == 0) {
        tokens[token_count] = NULL;
        execvp(tokens[0], tokens);

        exit(-1);
      } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status)) {
          last_exit_status = WEXITSTATUS(status);
        }
      } else {
        return_val = -1;
      }
    }

    if (return_val != 0 && return_val != 666) {
      perror(tokens[0]);
    }

    // close redirects
    if (redirect_out != -1) {
      dup2(redirect_out, 1);
      close(redirect_out);
      redirect_out = -1;
    }
    if (redirect_in != -1) {
      dup2(redirect_in, 0);
      close(redirect_in);
      redirect_in = -1; 
    }

    if (detach) {
      exit(0);
    }
  }
}

// preprosti ukazi
int name_cmd() {
  if (token_count > 1) {
    memcpy(shell_name, tokens[1], strlen(tokens[1]) + 1);
  } else {
    printf("%s\n", shell_name);
  }

  return 0;
}
int help_cmd() {
  printf("%s\n", "POMOC!");

  return 0;
}
int status_cmd() {
  printf("%d\n", last_exit_status);

  return 0;
}
int exit_cmd() {
  exit(atoi(tokens[1]));

  return 0;
}
int print_cmd() {
  for (int i = 1; i < token_count; i++) {
    printf("%s ", tokens[i]);
  }
  // undo last whitespace char
  printf("\e[D");

  return 0;
}
int echo_cmd() {
  print_cmd();
  printf("\n");

  return 0;
}
int pid_cmd() {
  printf("%d\n", getpid());
  
  return 0;
}
int ppid_cmd() {
  printf("%d\n", getppid());
  
  return 0;
}

// imeniki
int dirchange_cmd() {
  char *new_path;
  if (token_count > 1) new_path = tokens[1];
  else new_path = "/";

  return chdir(new_path);
}
int dirwhere_cmd() {
  char cwd[PATH_MAX],
    *ptr = getcwd(cwd, PATH_MAX);

  if (ptr != NULL) {
    printf("%s\n", cwd);
    return 0;
  } else {
    return -1;
  }
}
int dirmake_cmd() {
  return mkdir(tokens[1], 0755);
}
int dirremove_cmd() {
  return rmdir(tokens[1]);
}
int dirlist_cmd() {
  char *path = ".";
  if (token_count > 1) {
    path = tokens[1];
  }

  return list_files(path, -1);
}
int linkhard_cmd() {
  return link(tokens[1], tokens[2]);
}
int linksoft_cmd() {
  return symlink(tokens[1], tokens[2]);
}
int linkread_cmd() {
  char real_name[PATH_MAX];
  if (realpath(tokens[1], real_name) != NULL) {
    printf("%s\n", real_name);
  } else {
    return 1;
  }

  return 0;
}
int linklist_cmd() {
  struct stat st = {0};
  if (stat(tokens[1], &st) == 0) {
    list_files(".", st.st_ino);
    
    return 0;
  } else {
    return -1;
  }
}
int unlink_cmd() {
  return unlink(tokens[1]);
}
int rename_cmd() {
  return rename(tokens[1], tokens[2]);
}
int cpcat_cmd() {
  int read_fd = 0, write_fd = 1;
  // preusmerimo vhod
  if (token_count > 1 && strcmp(tokens[1], "-") != 0) {
    // printf("Redirecting input to %s\n", tokens[1]);
    read_fd = open(tokens[1], O_RDONLY);
    if (read_fd == -1)
      return  -1;
  }

  // preusmerimo izhod
  if (token_count > 2 && strcmp(tokens[2], "-") != 0) {
    // printf("Redirecting output to %s\n", tokens[2]);
    write_fd = open(tokens[2], O_WRONLY | O_CREAT, 0644);
    if (write_fd == -1)
      return  -1;
  }

  // vhod ima shared lock, izhod ima exclusive
  // ce je katerakoli datoteka ze zaklenjena, pa program caka, dokler ne dobi dostopa
  if (flock(0, LOCK_SH) == -1) {
    return -1;
  }
  if (flock(1, LOCK_EX) == -1) {
    return -1;
  }

  // "ne uprabljate copy&paste za vsak nacin uporabe argumentov"
  // ne razumem kaj naj bi to pomenilo, vedno je treba bite iz vhoda
  // kopirati na izhod
  char data[128];
  int bytes;
  while ((bytes = read(read_fd, data, 128)) > 0) {
    write(write_fd, data, bytes);
  }

  // obe kljucavnici se avtomatsko sprostita, ko se proces konca
  // torej ni potrebno izvesti sistemeskega klica za sprostitev
  if (flock(0, LOCK_UN) == -1) {
    return -1;
  }
  if (flock(1, LOCK_UN) == -1) {
    return -1;
  }

  if (read_fd != 0)
    close(read_fd);
  if (write_fd != 1)
    close(write_fd);

  return 0;
}

// ####################### Cevovod ########################
int pipeline() {
  int fds1[2], fds2[2], pids[token_count-1],
    i = 1, ukaz_count;
  char *ukaz[30];

  // zacetek ----------------------------------------------
  ukaz_count = tokenize(tokens[i], ukaz);
  if (pipe(fds1) == -1) return -1;

  pids[i-1] = fork();
  if (pids[i-1] == 0) {
    dup2(fds1[1], 1);
    close(fds1[0]);
    close(fds1[1]);

    // exec    
    ukaz[ukaz_count] = NULL;
    execvp(ukaz[0], ukaz);
    exit(-67);
  }

  // vmesne -----------------------------------------------
  for (i = 2; i < token_count-1; i++) {
    ukaz_count = tokenize(tokens[i], ukaz);
    if (pipe(fds2) == -1) return -1;

    pids[i-1] = fork();
    if (pids[i-1] == 0) {
      dup2(fds1[0], 0);
      dup2(fds2[1], 1);
      close(fds1[0]);
      close(fds1[1]);
      close(fds2[0]);
      close(fds2[1]);

      // exec
      ukaz[ukaz_count] = NULL;
      execvp(ukaz[0], ukaz);
      exit(-67);
    } else {
      close(fds1[0]);
      close(fds1[1]);
      fds1[0] = fds2[0];
      fds1[1] = fds2[1];
    }
  }

  // konec ------------------------------------------------
  ukaz_count = tokenize(tokens[i], ukaz);

  pids[i-1] = fork();
  if (pids[i-1] == 0) {
    dup2(fds1[0], 0);
    close(fds1[0]);
    close(fds1[1]);

    // exec
    ukaz[ukaz_count] = NULL;
    execvp(ukaz[0], ukaz);
    exit(-67);
  }
  close(fds1[0]);
  close(fds1[1]);

  int status;
  for (i = token_count-2; i >= 0; i--) {
    waitpid(pids[i], &status, 0);
  }

  return 0;
}



// ########################################################
// ####################### Helpers ########################
// ########################################################

int list_files(char *path, int inode) {
  struct dirent *dir;
  DIR *d = opendir(path);
  if (d != NULL) {
    while ((dir = readdir(d)) != NULL) {
      if (inode == -1 || dir->d_ino == inode)
        printf("%s  ", dir->d_name);
    }

    printf("\e[2D\n");
    closedir(d);
  } else {
    return -1;
  }

  return 0;
}

// split the read line into an array of strings
int tokenize(char *line, char **target) {
  int position = 0, count = 0, len = strlen(line);

  while (position < len) {
    // skip to next non whitespace
    while (isspace(line[position])) ++position;

    // check if EOF was encountered
    if (line[position] == '\0') break;

    // check if " was  encountered
    if (line[position] == '"') {
      target[count] = &line[++position];
      while (line[position] != '"') ++position;
    } else {
      target[count] = &line[position];
      while (isspace(line[position]) == 0) ++position;
    }

    line[position] = '\0';
    ++count;
    ++position;
  }
  
  return count;
}