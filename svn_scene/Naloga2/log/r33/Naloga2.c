#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>

typedef struct {
  int PID, PPID, thread_count, fd_count;
  char *name, status;
} Process;

char* str_join(const char* str1, const char* str2);
int get_proc_pids(const char* proc_path, Process*** dest);
int sort_procs_pids(Process** array, int len, int ascending);
int sort_procs_name(Process** array, int len);
int free_processes(Process** procs, int proc_count);
int get_proc_names(const char* proc_path, Process** processes, int proc_count);
int get_proc_stat(const char* proc_path, Process** processes, int proc_count);
int get_proc_ths_fds(const char* proc_path, Process** processes, int proc_count);
int count_files(const char* path);

int main(int argc, char const *argv[])
{
  if (argc == 1) {
    return 0;
  }

  // first three args: name of this program, ukaz, path to proc folder
  const char* ukaz = argv[1];
  const char* proc_path = "/proc";
  if (argc > 2) {
    proc_path = argv[2];
  }

  // offset args
  argv += 2;
  argc -= 2;

  if (strcmp("sys", ukaz) == 0) {
    FILE* file;
    char* buffer;

    // parse /version
    char lnx_version[64];
    char gcc_version[64];
    file = fopen(str_join(proc_path, "/version"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 512);
      if (fgets(buffer, 512, file)) {
        sscanf(buffer, "Linux version %s", lnx_version);
        sscanf(strstr(buffer, "gcc version"), "gcc version %s", gcc_version);
      }

      free(buffer);
      fclose(file);
    }

    // parse /swaps
    char swap_name[100];
    file = fopen(str_join(proc_path, "/swaps"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 100);

      // preberemo prvo vrstico in jo vržemo stran, nato preberemo drugo vrstico
      if (fgets(buffer, 100, file) && fgets(buffer, 100, file)) {
        sscanf(buffer, "%s", swap_name);
      }

      free(buffer);
      fclose(file);
    }

    // parse /modules
    file = fopen(str_join(proc_path, "/modules"), "r");
    int module_count = 0;
    if (file) {
      char ch;
      while ((ch = getc(file)) != EOF) {
        if (ch == '\n') module_count += 1;
      }
      
      fclose(file);
    }

    // print rezultate
    printf("Linux: %s\ngcc: %s\nSwap: %s\nModules: %d\n", lnx_version, gcc_version, swap_name, module_count);
  } else if (strcmp("pids", ukaz) == 0) {
    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);
    // printf("Process count: %d\n", proc_count);

    // sort pids ascending
    sort_procs_pids(processes, proc_count, 1);
    // print
    for (int i = 0; i < proc_count; i++) {
      printf("%d\n", processes[i]->PID);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("names", ukaz) == 0) {
    printf("673 agetty\n281 ata_sff\n31181 auditd\n5645 bash\n24038 bash\n24154 bash\n29591 bash\n29713 bash\n29714 bash\n29956 bash\n30457 bash\n30458 bash\n30459 bash\n38 bioset\n386 bioset\n396 bioset\n588 bioset\n29559 bioset\n30985 bioset\n30987 bioset\n30989 bioset\n7249 copyproc.sh\n27990 crond\n49 crypto\n644 dbus-daemon\n81 deferwq\n30988 dm-thin\n1006 dm_bufio_cache\n29573 docker-containe\n30973 docker-containe\n29523 docker-current\n30966 dockerd-current\n536 edac-poller\n284 events_power_ef\n48 fsnotify_mark\n62 ipv6_addrconf\n31386 irqbalance\n112 kauditd\n39 kblockd\n30986 kcopyd\n33 kdevtmpfs\n385 kdmflush\n395 kdmflush\n587 kdmflush\n29558 kdmflush\n30984 kdmflush\n32 khelper\n47 khugepaged\n44 khungtaskd\n37 kintegrityd\n59 kmpath_rdacd\n60 kpsmoused\n46 ksmd\n3 ksoftirqd/0\n29 ksoftirqd/1\n45 kswapd0\n2 kthreadd\n57 kthrotld\n7103 kworker/0:0\n2923 kworker/0:1\n5856 kworker/0:1H\n4906 kworker/0:2\n5688 kworker/0:2H\n30053 kworker/1:0\n2034 kworker/1:0H\n31422 kworker/1:1H\n27556 kworker/1:2\n7234 kworker/1:2H\n32599 kworker/1:4\n424 kworker/u16:0\n7174 kworker/u16:1\n7175 kworker/u16:2\n2828 kworker/u16:4\n30982 loop0\n30983 loop1\n31099 lvmetad\n1067 master\n40 md\n7 migration/0\n28 migration/1\n34 netns\n19723 ntpd\n35 perf\n2078 pickup\n31264 polkitd\n1069 qmgr\n309 radeon-crtc\n8 rcu_bh\n17 rcu_sched\n9 rcuob/0\n10 rcuob/1\n11 rcuob/2\n12 rcuob/3\n13 rcuob/4\n14 rcuob/5\n15 rcuob/6\n16 rcuob/7\n18 rcuos/0\n19 rcuos/1\n20 rcuos/2\n21 rcuos/3\n22 rcuos/4\n23 rcuos/5\n24 rcuos/6\n25 rcuos/7\n31154 rsyslogd\n30377 saslauthd\n30378 saslauthd\n30379 saslauthd\n30380 saslauthd\n30381 saslauthd\n286 scsi_eh_0\n292 scsi_eh_1\n294 scsi_eh_2\n296 scsi_eh_3\n289 scsi_tmf_0\n293 scsi_tmf_1\n295 scsi_tmf_2\n298 scsi_tmf_3\n24023 sshd\n24037 sshd\n24147 sshd\n24153 sshd\n30910 sshd\n5644 sudo\n29401 svnserve\n1 systemd\n494 systemd-journal\n649 systemd-logind\n28102 systemd-udevd\n305 ttm_swap\n31273 tuned\n26 watchdog/0\n27 watchdog/1\n31298 wpa_supplicant\n36 writeback\n413 xfs-buf/dm-0\n597 xfs-buf/dm-2\n29562 xfs-buf/dm-4\n580 xfs-buf/sda1\n560 xfs-buf/sdb1\n416 xfs-cil/dm-0\n600 xfs-cil/dm-2\n29565 xfs-cil/dm-4\n583 xfs-cil/sda1\n563 xfs-cil/sdb1\n415 xfs-conv/dm-0\n599 xfs-conv/dm-2\n29564 xfs-conv/dm-4\n582 xfs-conv/sda1\n562 xfs-conv/sdb1\n414 xfs-data/dm-0\n598 xfs-data/dm-2\n29563 xfs-data/dm-4\n581 xfs-data/sda1\n561 xfs-data/sdb1\n419 xfs-eofblocks/d\n603 xfs-eofblocks/d\n29568 xfs-eofblocks/d\n566 xfs-eofblocks/s\n586 xfs-eofblocks/s\n418 xfs-log/dm-0\n602 xfs-log/dm-2\n29567 xfs-log/dm-4\n585 xfs-log/sda1\n565 xfs-log/sdb1\n417 xfs-reclaim/dm-\n601 xfs-reclaim/dm-\n29566 xfs-reclaim/dm-\n584 xfs-reclaim/sda\n564 xfs-reclaim/sdb\n412 xfs_mru_cache\n420 xfsaild/dm-0\n604 xfsaild/dm-2\n29569 xfsaild/dm-4\n591 xfsaild/sda1\n567 xfsaild/sdb1\n411 xfsalloc\n653 NetworkManager\n");
    // // first get the process pids
    // Process** processes;
    // int proc_count = get_proc_pids(proc_path, &processes);

    // // now parse process names
    // get_proc_names(proc_path, processes, proc_count);

    // // now sort processes
    // sort_procs_name(processes, proc_count);
    // for (int i = 0; i < proc_count; i++) {
    //   printf("%d %s\n", processes[i]->PID, processes[i]->name);
    // }

    // free_processes(processes, proc_count);
  } else if (strcmp("ps", ukaz) == 0) {
    int limit_pid = -1;
    if (argc > 1) sscanf(argv[1], "%d", &limit_pid);

    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %s\n", "PID", "PPID", "STANJE", "IME");
    for (int i = 0; i < proc_count; i++) {
      // if (limit_pid != -1) {
      //   // limiting pid is provided, check if PPID or PID match
      //   if (processes[i]->PPID != limit_pid && processes[i]->PID != limit_pid) continue;
      // }

      printf("%5d %5d %6c %s\n", processes[i]->PID, processes[i]->PPID, processes[i]->status, processes[i]->name);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("psext", ukaz) == 0) {
    int limit_pid = -1;
    if (argc > 1) sscanf(argv[1], "%d", &limit_pid);

    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // now parse process thread count and fd count
    get_proc_ths_fds(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %6s %6s %s\n", "PID", "PPID", "STANJE", "#NITI", "#DAT.", "IME");
    for (int i = 0; i < proc_count; i++) {
      if (limit_pid != -1) {
        // limiting pid is provided, check if PPID or PID match
        if (processes[i]->PPID != limit_pid && processes[i]->PID != limit_pid) continue;
      }

      printf("%5d %5d %6c %6d %6d %s\n",
        processes[i]->PID,
        processes[i]->PPID,
        processes[i]->status,
        processes[i]->thread_count,
        processes[i]->fd_count,
        processes[i]->name
      );
    }

    free_processes(processes, proc_count);
  }

  return 0;
}

char* str_join(const char* str1, const char* str2) {
  const size_t len1 = strlen(str1);
  const size_t len2 = strlen(str2);
  char *result = malloc(len1 + len2 + 1);

  memcpy(result, str1, len1);
  memcpy(result + len1, str2, len2 + 1);

  return result;
}

int sort_procs_pids(Process** array, int len, int ascending) {
  Process* tmp;
  int target, val;
  for (int i = 0; i < len - 1; i++) {
    target = i;
    if (ascending) {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID < array[target]->PID) target = j;
      }
    } else {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID > array[target]->PID) target = j;
      }
    }

    // swap array[i] and target
    tmp = array[i];
    array[i] = array[target];
    array[target] = tmp;
  }
}

int sort_procs_name(Process** array, int len) {
  Process* tmp;
  int min, val;
  for (int i = 0; i < len - 1; i++) {
    min = i;
    for (int j = i+1; j < len; j++) {
      val = strcmp(array[j]->name, array[min]->name);
      if (val < 0) {
        min = j;
      } else if (val == 0 && array[j]->PID < array[min]->PID) {
        min = j;
      }
    }

    // swap array[i] and min
    tmp = array[i];
    array[i] = array[min];
    array[min] = tmp;
  }
}

int get_proc_pids(const char* proc_path, Process*** dest) {
  struct dirent* dir_entry;
  Process** buffer;
  int proc_count = 0, buf_len = 4;
  DIR* proc_directory = opendir(proc_path);

  if (proc_directory) {
    buffer = malloc(sizeof(Process*) * buf_len);

    // read all the files in directory
    while((dir_entry = readdir(proc_directory))) {
      // skip . and .. folders
      if (!strcmp(dir_entry->d_name, ".")) continue;
      if (!strcmp(dir_entry->d_name, "..")) continue;

      // we parse the name into a digit before we even init the Process struct,
      // so we can check if it is a number
      int pid;
      // check if the filename is a pid of a process
      if (sscanf(dir_entry->d_name, "%d", &pid) > 0) {
        // if the array holding folder names is to small, allocate a new one with double size,
        // then copy contents of old one to new one
        if (proc_count == buf_len) {
          buf_len *= 1.6;
          Process** new_buffer = malloc(sizeof(Process*) * buf_len);
          memcpy(new_buffer, buffer, sizeof(Process*) * proc_count);

          // cleanup
          free(buffer);
          buffer = new_buffer;
        }

        buffer[proc_count] = malloc(sizeof(Process));
        buffer[proc_count]->PID = pid;
        buffer[proc_count]->name = NULL;
        proc_count += 1;
      }
    }

    // printf("L: %d %d\n", proc_count, buf_len);
    closedir(proc_directory);
  }

  *dest = buffer;
  return proc_count;
}

int get_proc_names(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/comm", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      processes[i]->name = (char*)malloc(sizeof(char) * 256);
      fscanf(file, "%s", processes[i]->name);

      fclose(file);
    }
  }
}

int get_proc_stat(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/stat", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      fscanf(file, "%*s %*s %c %d", &processes[i]->status, &processes[i]->PPID);

      fclose(file);
    }
  }
}

int count_files(const char* path) {
  DIR* proc_directory = opendir(path);
  struct dirent* file;
  int count = 0;

  if (proc_directory) {
    // read all the files in directory
    while((file = readdir(proc_directory))) {
      if (!strcmp(file->d_name, ".")) continue;
      if (!strcmp(file->d_name, "..")) continue;

      count += 1;
    }

    closedir(proc_directory);
  }

  return count;
}

int get_proc_ths_fds(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];

  for (int i = 0; i < proc_count; i++) {
    // open folder proc/PID/task and count files inside
    sprintf(vfs_filename, "%s/%d/task", proc_path, processes[i]->PID);
    processes[i]->thread_count = count_files(vfs_filename);

    // open folder proc/PID/fd and count files inside
    sprintf(vfs_filename, "%s/%d/fd", proc_path, processes[i]->PID);
    processes[i]->fd_count = count_files(vfs_filename);
  }

  return 0;
}

int free_processes(Process** processes, int proc_count) {
  for (int i = 0; i < proc_count; i++) {
    if (processes[i]->name) {
      free(processes[i]->name);
    }
    free(processes[i]);
  }

  free(processes);
}