#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>

typedef struct {
  char print;
  int PID, PPID, thread_count, fd_count;
  char *name, status;
} Process;

typedef Process** ProcList;

char* str_join(const char* str1, const char* str2);
int get_proc_pids(const char* proc_path, ProcList* dest);
void sort_procs_pids(ProcList array, int len, int ascending);
void sort_procs_name(ProcList array, int len);
void free_processes(ProcList procs, int proc_count);
void get_proc_names(const char* proc_path, ProcList processes, int proc_count);
void get_proc_stat(const char* proc_path, ProcList processes, int proc_count);
void get_proc_fds(const char* proc_path, ProcList processes, int proc_count);
int count_files(const char* path);
int is_child(int cidx, int tpid, ProcList processes, int proc_count);
void trimProcessess(int ancestor_pid, ProcList processes, int proc_count);

int main(int argc, char const *argv[]) {
  if (argc == 1)
    return 0;

  // first three args: name of this program, ukaz, path to proc folder
  const char* ukaz = argv[1];
  const char* proc_path = "/proc";
  if (argc > 2)
    proc_path = argv[2];

  // offset args
  argv += 2;
  argc -= 2;

  if (strcmp("sys", ukaz) == 0) {
    FILE* file;
    char* buffer;

    // parse /version
    char lnx_version[64];
    char gcc_version[64];
    file = fopen(str_join(proc_path, "/version"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 512);
      if (fgets(buffer, 512, file)) {
        sscanf(buffer, "Linux version %s", lnx_version);
        sscanf(strstr(buffer, "gcc version"), "gcc version %s", gcc_version);
      }

      free(buffer);
      fclose(file);
    }

    // parse /swaps
    char swap_name[100];
    file = fopen(str_join(proc_path, "/swaps"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 100);

      // preberemo prvo vrstico in jo vržemo stran, nato preberemo drugo vrstico
      if (fgets(buffer, 100, file) && fgets(buffer, 100, file)) {
        sscanf(buffer, "%s", swap_name);
      }

      free(buffer);
      fclose(file);
    }

    // parse /modules
    file = fopen(str_join(proc_path, "/modules"), "r");
    int module_count = 0;
    if (file) {
      char ch;
      while ((ch = getc(file)) != EOF) {
        if (ch == '\n') module_count += 1;
      }
      
      fclose(file);
    }

    // print rezultate
    printf("Linux: %s\ngcc: %s\nSwap: %s\nModules: %d\n", lnx_version, gcc_version, swap_name, module_count);
  } else if (strcmp("pids", ukaz) == 0) {
    // first get the process pids
    ProcList processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // sort pids ascending
    sort_procs_pids(processes, proc_count, 1);

    // print
    for (int i = 0; i < proc_count; i++) {
      printf("%d\n", processes[i]->PID);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("names", ukaz) == 0) {
    // first get the process pids
    ProcList processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_name(processes, proc_count);

    // print
    for (int i = 0; i < proc_count; i++) {
      printf("%d %s\n", processes[i]->PID, processes[i]->name);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("ps", ukaz) == 0) {
    
    // first get the process pids
    ProcList processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // check if we need to print only children of a specific PID
    if (argc > 1) {
      int ancestor;
      sscanf(argv[1], "%d", &ancestor);
      trimProcessess(ancestor, processes, proc_count);
    }

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %s\n", "PID", "PPID", "STANJE", "IME");
    for (int i = 0; i < proc_count; i++) {
      if (argc > 1 && processes[i]->print != 'T')
        continue;

      printf("%5d %5d %6c %s\n", processes[i]->PID, processes[i]->PPID, processes[i]->status, processes[i]->name);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("psext", ukaz) == 0) {
    // first get the process pids
    ProcList processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // now parse process thread count and fd count
    get_proc_fds(proc_path, processes, proc_count);

    // check if we need to print only children of a specific PID
    int ancestor;
    if (argc > 1) {
      sscanf(argv[1], "%d", &ancestor);
      trimProcessess(ancestor, processes, proc_count);
    }

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %6s %6s %s\n", "PID", "PPID", "STANJE", "#NITI", "#DAT.", "IME");
    for (int i = 0; i < proc_count; i++) {
      if (argc > 1 && processes[i]->print != 'T' && processes[i]->PID != ancestor)
        continue;

      printf("%5d %5d %6c %6d %6d %s\n",
        processes[i]->PID,
        processes[i]->PPID,
        processes[i]->status,
        processes[i]->thread_count,
        processes[i]->fd_count,
        processes[i]->name
      );
    }

    free_processes(processes, proc_count);
  }

  return 0;
}

char* str_join(const char* str1, const char* str2) {
  const size_t len1 = strlen(str1);
  const size_t len2 = strlen(str2);
  char *result = malloc(len1 + len2 + 1);

  memcpy(result, str1, len1);
  memcpy(result + len1, str2, len2 + 1);

  return result;
}

void sort_procs_pids(ProcList array, int len, int ascending) {
  Process* tmp;
  int target, val;
  for (int i = 0; i < len - 1; i++) {
    target = i;
    if (ascending) {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID < array[target]->PID) target = j;
      }
    } else {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID > array[target]->PID) target = j;
      }
    }

    // swap array[i] and target
    tmp = array[i];
    array[i] = array[target];
    array[target] = tmp;
  }
}

void sort_procs_name(ProcList array, int len) {
  Process* tmp;
  int min, val;
  for (int i = 0; i < len - 1; i++) {
    min = i;
    for (int j = i+1; j < len; j++) {
      val = strcmp(array[j]->name, array[min]->name);
      if (val < 0) {
        min = j;
      } else if (val == 0 && array[j]->PID < array[min]->PID) {
        min = j;
      }
    }

    // swap array[i] and min
    tmp = array[i];
    array[i] = array[min];
    array[min] = tmp;
  }
}

int get_proc_pids(const char* proc_path, ProcList* dest) {
  struct dirent* dir_entry;
  ProcList buffer;
  int proc_count = 0, buf_len = 4, pid;
  DIR* proc_directory = opendir(proc_path);

  if (proc_directory) {
    buffer = malloc(sizeof(Process*) * buf_len);

    // read all the files in directory
    while((dir_entry = readdir(proc_directory))) {
      // skip . and .. folders
      if (!strcmp(dir_entry->d_name, ".")) continue;
      if (!strcmp(dir_entry->d_name, "..")) continue;

      // we parse the name into a digit before we even init the Process struct,
      // so we can check if it is a number
      if (sscanf(dir_entry->d_name, "%d", &pid) > 0) {
        // if the array holding folder names is to small, allocate a new one with double size,
        // then copy contents of old one to new one
        if (proc_count == buf_len) {
          buf_len *= 1.6;
          ProcList new_buffer = malloc(sizeof(Process*) * buf_len);
          memcpy(new_buffer, buffer, sizeof(Process*) * proc_count);

          // cleanup
          free(buffer);
          buffer = new_buffer;
        }

        buffer[proc_count] = malloc(sizeof(Process));
        buffer[proc_count]->PID = pid;
        buffer[proc_count]->name = NULL;
        proc_count += 1;
      }
    }

    closedir(proc_directory);
  }

  *dest = buffer;
  return proc_count;
}

void get_proc_names(const char* proc_path, ProcList processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/comm", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      processes[i]->name = (char*)malloc(sizeof(char) * 256);
      fscanf(file, "%s", processes[i]->name);

      fclose(file);
    }
  }
}

void get_proc_stat(const char* proc_path, ProcList processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/stat", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      fscanf(file, "%*s %*s %c %d", &processes[i]->status, &processes[i]->PPID);
      for (int j = 0; j < 15; j++)
        fscanf(file, "%*s");
      fscanf(file, "%d", &processes[i]->thread_count);

      fclose(file);
    }
  }
}

int count_files(const char* path) {
  return 0;
}

void get_proc_fds(const char* proc_path, ProcList processes, int proc_count) {
  char vfs_filename[128];
  DIR* proc_directory;
  struct dirent* file;

  for (int i = 0; i < proc_count; i++) {
    // open folder proc/PID/fd and count files inside
    sprintf(vfs_filename, "%s/%d/fd", proc_path, processes[i]->PID);
    proc_directory = opendir(vfs_filename);
    processes[i]->fd_count = 0;

    if (proc_directory) {
      // read all the files in directory
      while((file = readdir(proc_directory))) {
        if (!strcmp(file->d_name, ".")) continue;
        if (!strcmp(file->d_name, "..")) continue;

        processes[i]->fd_count += 1;
      }

      closedir(proc_directory);
    }

  }
}

void _trimProcessess(int PPID, ProcList processes, int proc_count) {
  for (int i = 0; i < proc_count; i++) {
    if (processes[i]->PPID == PPID) {
      processes[i]->print = 'T';
      _trimProcessess(processes[i]->PID, processes, proc_count);
    }
  }
}
void trimProcessess(int ancestor, ProcList processes, int proc_count) {
  for (int i = 0; i < proc_count; i++) {
    if (processes[i]->PID == ancestor) {
      processes[i]->print = 'T';
      break;
    }
  }

  _trimProcessess(ancestor, processes, proc_count);
}

void free_processes(ProcList processes, int proc_count) {
  for (int i = 0; i < proc_count; i++) {
    if (processes[i]->name) {
      free(processes[i]->name);
    }
    free(processes[i]);
  }

  free(processes);
}