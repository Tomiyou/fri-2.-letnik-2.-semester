#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>

typedef struct {
  int PID, PPID, thread_count, fd_count;
  char *name, status;
} Process;

char* str_join(const char* str1, const char* str2);
int get_proc_pids(const char* proc_path, Process*** dest);
int sort_procs_pids(Process** array, int len, int ascending);
int sort_procs_name(Process** array, int len);
int free_processes(Process** procs, int proc_count);
int get_proc_names(const char* proc_path, Process** processes, int proc_count);
int get_proc_stat(const char* proc_path, Process** processes, int proc_count);
int get_proc_ths_fds(const char* proc_path, Process** processes, int proc_count);
int count_files(const char* path);

int main(int argc, char const *argv[])
{
  if (argc == 1) {
    return 0;
  }

  // first three args: name of this program, ukaz, path to proc folder
  const char* ukaz = argv[1];
  const char* proc_path = "/proc";
  if (argc > 2) {
    proc_path = argv[2];
  }

  // offset args
  argv += 2;
  argc -= 2;

  if (strcmp("sys", ukaz) == 0) {
    FILE* file;
    char* buffer;

    // parse /version
    char lnx_version[64];
    char gcc_version[64];
    file = fopen(str_join(proc_path, "/version"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 512);
      if (fgets(buffer, 512, file)) {
        sscanf(buffer, "Linux version %s", lnx_version);
        sscanf(strstr(buffer, "gcc version"), "gcc version %s", gcc_version);
      }

      free(buffer);
      fclose(file);
    }

    // parse /swaps
    char swap_name[100];
    file = fopen(str_join(proc_path, "/swaps"), "r");
    if (file) {
      buffer = malloc(sizeof(char) * 100);

      // preberemo prvo vrstico in jo vržemo stran, nato preberemo drugo vrstico
      if (fgets(buffer, 100, file) && fgets(buffer, 100, file)) {
        sscanf(buffer, "%s", swap_name);
      }

      free(buffer);
      fclose(file);
    }

    // parse /modules
    file = fopen(str_join(proc_path, "/modules"), "r");
    int module_count = 0;
    if (file) {
      char ch;
      while ((ch = getc(file)) != EOF) {
        if (ch == '\n') module_count += 1;
      }
      
      fclose(file);
    }

    // print rezultate
    printf("Linux: %s\ngcc: %s\nSwap: %s\nModules: %d\n", lnx_version, gcc_version, swap_name, module_count);
  } else if (strcmp("pids", ukaz) == 0) {
    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);
    printf("Process count: %d\n", proc_count);

    // sort pids ascending
    sort_procs_pids(processes, proc_count, 1);
    // print
    for (int i = 0; i < proc_count; i++) {
      printf("%d\n", processes[i]->PID);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("names", ukaz) == 0) {
    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_name(processes, proc_count);
    for (int i = 0; i < proc_count; i++) {
      printf("%d %s\n", processes[i]->PID, processes[i]->name);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("ps", ukaz) == 0) {
    int limit_pid = -1;
    if (argc > 1) sscanf(argv[1], "%d", &limit_pid);

    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %s\n", "PID", "PPID", "STANJE", "IME");
    for (int i = 0; i < proc_count; i++) {
      if (limit_pid != -1) {
        // limiting pid is provided, check if PPID or PID match
        if (processes[i]->PPID != limit_pid && processes[i]->PID != limit_pid) continue;
      }

      printf("%5d %5d %6c %s\n", processes[i]->PID, processes[i]->PPID, processes[i]->status, processes[i]->name);
    }

    free_processes(processes, proc_count);
  } else if (strcmp("psext", ukaz) == 0) {
    int limit_pid = -1;
    if (argc > 1) sscanf(argv[1], "%d", &limit_pid);

    // first get the process pids
    Process** processes;
    int proc_count = get_proc_pids(proc_path, &processes);

    // now parse process names
    get_proc_names(proc_path, processes, proc_count);

    // now parse process PPID in status
    get_proc_stat(proc_path, processes, proc_count);

    // now parse process thread count and fd count
    get_proc_ths_fds(proc_path, processes, proc_count);

    // now sort processes
    sort_procs_pids(processes, proc_count, 1);

    // print ps
    printf("%5s %5s %6s %6s %6s %s\n", "PID", "PPID", "STANJE", "#NITI", "#DAT.", "IME");
    for (int i = 0; i < proc_count; i++) {
      if (limit_pid != -1) {
        // limiting pid is provided, check if PPID or PID match
        if (processes[i]->PPID != limit_pid && processes[i]->PID != limit_pid) continue;
      }

      printf("%5d %5d %6c %6d %6d %s\n",
        processes[i]->PID,
        processes[i]->PPID,
        processes[i]->status,
        processes[i]->thread_count,
        processes[i]->fd_count,
        processes[i]->name
      );
    }

    free_processes(processes, proc_count);
  }

  return 0;
}

char* str_join(const char* str1, const char* str2) {
  const size_t len1 = strlen(str1);
  const size_t len2 = strlen(str2);
  char *result = malloc(len1 + len2 + 1);

  memcpy(result, str1, len1);
  memcpy(result + len1, str2, len2 + 1);

  return result;
}

int sort_procs_pids(Process** array, int len, int ascending) {
  Process* tmp;
  int target, val;
  for (int i = 0; i < len - 1; i++) {
    target = i;
    if (ascending) {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID < array[target]->PID) target = j;
      }
    } else {
      for (int j = i+1; j < len; j++) {
        if (array[j]->PID > array[target]->PID) target = j;
      }
    }

    // swap array[i] and target
    tmp = array[i];
    array[i] = array[target];
    array[target] = tmp;
  }
}

int sort_procs_name(Process** array, int len) {
  Process* tmp;
  int min, val;
  for (int i = 0; i < len - 1; i++) {
    min = i;
    for (int j = i+1; j < len; j++) {
      val = strcmp(array[j]->name, array[min]->name);
      if (val < 0) {
        min = j;
      } else if (val == 0 && array[j]->PID < array[min]->PID) {
        min = j;
      }
    }

    // swap array[i] and min
    tmp = array[i];
    array[i] = array[min];
    array[min] = tmp;
  }
}

int get_proc_pids(const char* proc_path, Process*** dest) {
  struct dirent* file;
  Process** buffer;
  int proc_count = 0, buf_len = 4;
  DIR* proc_directory = opendir(proc_path);

  if (proc_directory) {
    printf("Directory does exist. %s\n", proc_path);
    buffer = malloc(sizeof(Process*) * buf_len);

    // read all the files in directory
    while((file = readdir(proc_directory))) {
      printf("File: %s\n", file->d_name);
      // file->d_type == 4 -> file is a folder
      if (file->d_type == 4) {
        // skip . and .. folders
        if (!strcmp(file->d_name, ".")) continue;
        if (!strcmp(file->d_name, "..")) continue;


        // if the array holding folder names is to small, allocate a new one with double size,
        // then copy contents of old one to new one
        if (proc_count == buf_len) {
          buf_len *= 1.6;
          Process** new_buffer = malloc(sizeof(Process*) * buf_len);
          memcpy(new_buffer, buffer, sizeof(Process*) * proc_count);

          // cleanup
          free(buffer);
          buffer = new_buffer;
        }

        buffer[proc_count] = malloc(sizeof(Process));
        if (sscanf(file->d_name, "%d", &buffer[proc_count]->PID) > 0) {
          proc_count += 1;
        } else {
          free(buffer[proc_count]);
        }

      }
    }

    // printf("L: %d %d\n", proc_count, buf_len);
    closedir(proc_directory);
  } else {
    printf("Proc directory is a NULL pointer.\n");
  }

  *dest = buffer;
  return proc_count;
}

int get_proc_names(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/comm", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      processes[i]->name = (char*)malloc(sizeof(char) * 256);
      fscanf(file, "%s", processes[i]->name);

      fclose(file);
    }
  }
}

int get_proc_stat(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];
  FILE* file;

  for (int i = 0; i < proc_count; i++) {
    // read process status and PPID from proc/PID/stat
    sprintf(vfs_filename, "%s/%d/stat", proc_path, processes[i]->PID);
    file = fopen(vfs_filename, "r");

    if (file) {
      fscanf(file, "%*s %*s %c %d", &processes[i]->status, &processes[i]->PPID);

      fclose(file);
    }
  }
}

int count_files(const char* path) {
  DIR* proc_directory = opendir(path);
  struct dirent* file;
  int count = 0;

  if (proc_directory) {
    // read all the files in directory
    while((file = readdir(proc_directory))) {
      if (!strcmp(file->d_name, ".")) continue;
      if (!strcmp(file->d_name, "..")) continue;

      count += 1;
    }

    closedir(proc_directory);
  }

  return count;
}

int get_proc_ths_fds(const char* proc_path, Process** processes, int proc_count) {
  char vfs_filename[128];

  for (int i = 0; i < proc_count; i++) {
    // open folder proc/PID/task and count files inside
    sprintf(vfs_filename, "%s/%d/task", proc_path, processes[i]->PID);
    processes[i]->thread_count = count_files(vfs_filename);

    // open folder proc/PID/fd and count files inside
    sprintf(vfs_filename, "%s/%d/fd", proc_path, processes[i]->PID);
    processes[i]->fd_count = count_files(vfs_filename);
  }

  return 0;
}

int free_processes(Process** processes, int proc_count) {
  for (int i = 0; i < proc_count; i++) {
    if (processes[i]->name) {
      free(processes[i]->name);
    }
    free(processes[i]);
  }

  free(processes);
}