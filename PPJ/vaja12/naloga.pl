:- use_module(library(clpfd))

meeting(X, Y) :-
  X #>= 0,
  Y #>= 0,
  X + Y #= 22,
  2*X + 4*Y #= 72.