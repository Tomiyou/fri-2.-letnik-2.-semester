type height = int
type value = int

type avltree =
  | Empty
  | Node of value * height * avltree * avltree

let moje_drevo =
  Node (5, 3, 
    Node (3, 2,
      Node(1, 1, Empty, Empty),
      Node(4, 1, Empty, Empty)
    ),
    Node (8, 1, Empty, Empty)
  )

let get_height = function
  | Empty -> 0
  | Node (_, h, _, _) -> h

let make_leaf v = Node (v, 1, Empty, Empty)

let naredi_node (v, l, r) = Node (v, 1 + max (get_height l) (get_height r), l, r)

let moje_drevo' =
  naredi_node (5,
    naredi_node (3,
      naredi_node (1, Empty, Empty),
      naredi_node (4, Empty, Empty)
    ),
    naredi_node (8, Empty, Empty)
  )

let moje_drevo'' = naredi_node

let rec toList = function
  | Empty -> []
  | (Node (x, _, l, r)) -> toList l @ [x] @ toList r

type order = Less | Equal | Greater

let cmpNum x y =
  match compare x y with
  | 0 -> Equal
  | r when r < 0 -> Less
  | _ -> Greater

(* prvi argument je stevilo, drugi argument je avltree, vrne pa bool *)
let rec najdi x = function
  | Empty -> false
  | Node (y, _, l, r) ->
    begin match cmpNum x y with
    | Equal -> true
    | Less -> najdi x l
    | Greater -> najdi x r
    end

let test1 = najdi 1 moje_drevo
let test2 = najdi 42 moje_drevo

let rotateLeft = function
  | Node (x, _, a, Node (y, _, b, c)) -> naredi_node (y, naredi_node (x, a, b), c)
  | t -> t

let rotateRight = function
  | Node (y, _, Node (x, _, a, b), c) -> naredi_node (x, a, naredi_node (y, b, c))
  | t -> t

let razlikaVisine = function
  | Empty -> 0
  | Node (_, _, l, r) -> get_height l - get_height r

let balance = function
  | Empty -> Empty
  | Node (x, _, l, r) as t ->
      begin match razlikaVisine t with
      | 2 ->
          begin match razlikaVisine l with
          | -1 -> rotateRight (naredi_node (x, rotateLeft l, r))
          | _ -> rotateRight t
          end
      | -2 ->
          begin match razlikaVisine r with
          | 1 -> rotateLeft (naredi_node (x, l, rotateRight r))
          | _ -> rotateLeft t
          end
      | _ -> t
      end

let rec dodaj x = function
  | Empty -> make_leaf x
  | Node (y, _, l, r) as t ->
      begin match cmpNum x y with
      | Equal -> t
      | Less -> balance (naredi_node (y, dodaj x l, r))
      | Greater -> balance (naredi_node (y, l, dodaj x r))
      end

let rec remove x = function
  | Empty -> Empty
  | Node (y, _, l, r) ->
      let rec removeSuccessor = function
        | Empty -> failwith "impossible"
        | Node (x, _, Empty, r) -> (r, x)
        | Node (x, _, l, r) ->
            let (l', y) = removeSuccessor l in
            (balance (naredi_node (x, l', r)), y)
      in
      match cmpNum x y with
      | Less -> balance (naredi_node (y, remove x l, r))
      | Greater -> balance (naredi_node (y, l, remove x r))
      | Equal ->
          begin match (l, r) with
          | (_, Empty) -> l
          | (Empty, _) -> r
          | _ -> let (r', y') = removeSuccessor r in
            balance (naredi_node (y', l, r'))
          end
