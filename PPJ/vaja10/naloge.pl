mother(X, Y) :-
  parent(X, Y),
  female(X).

grandparent(X, Y) :-
  parent(X, Z),
  parent(Z, Y).

sister(X, Y) :-
  female(X),
  dif(X, Y),
  parent(P, X),
  parent(P, Y).

aunt(X, Y) :-
  parent(Z, Y),
  sister(X, Z).

% najprej ustavitven pogoj
ancestor(X, Y) :-
  parent(X, Y);
  parent(P, Y),
  ancestor(X, P).

descendant(X, Y) :-
  ancestor(Y, X).

% ce velja, da lahko seznam dam v H in T, potem lahko appendam T in B
append1([], B, B).
append1([H|T], B, [H | TinB]) :-
  append1(T, B, TinB).


ancestor(X, Y, [X, Y]) :-
  parent(X, Y).

ancestor(X, Y, Pot) :-
  parent(X, Z),
  ancestor(Z, Y, PotZY),
  append([X], PotZY, Pot).