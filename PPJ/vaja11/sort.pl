program(copy, q0, b, final, b, stay).
program(copy, q0, 1, 2, b, right).
program(copy, 2, b, 3, b, right).
program(copy, 2, 1, 2, 1, right).
program(copy, 3, b, 4, 1, left).
program(copy, 3, 1, 3, 1, right).
program(copy, 4, b, 5, b, left).
program(copy, 4, 1, 4, 1, left).
program(copy, 5, b, q0, 1, right).
program(copy, 5, 1, 5, 1, left).


program(plus1, q0, 1, q0, 1, right).
program(plus1, q0, b, final, 1, stay).



% samo za takrat, ko dobimo [] kot vhod (sicer se ustavi že pri [A])
is_sorted([]).
is_sorted([A]).
is_sorted([A, B | T]):-
  A =< B,
  is_sorted([B | T]).

%  -------------------------------------------------
%  Od zadnjič
insert(X, L, [X|L]).
insert(X, [H|T], [H|T2]) :-
  insert(X, T, T2).

reverse([], []).
reverse([H|T], L) :-
  reverse(T, R),
  append(R, [H], L). % za dodajanje na konec seznama si pomagamo z append/3

permute([], []).
permute([H | T], L):-
  permute(T, TP),
  insert(H, TP, L).
%  -------------------------------------------------


bogosort(In, Out):-
  permute(In, Out),
  is_sorted(Out).


% Tape je par seznamov
% action(Direction, InTape, OutTape)
action(stay, InL-InR, InL-InR).
action(right, InL-[A|B], [A|InL]-B).
action(left, [A|B]-InR, B-[A|InR]).
action(left, []-InR, []-[b|InR]).

head_rest([], b, []).
head_rest([H | T], H, T).

step(Program, InL-InR, InState, OutL-OutR, OutState):-
  % poisci simbol pod glavo
  head_rest(InR, glava, preostanek),

  % poisci pravilo za program glede na vhodno stanje in simbol
  % dobi novo stanje, nov simbol in premik
  program(Program, InState, glava, OutState, nova_glava, Direction),

  % popravi trak InL-InR (zamenjaj simbol, premakni glavo)
  NewTape = InL-[nova_glava|preostanek],

  % dobi OutL-OutR
  action(Direction, NewTape, OutL-OutR).

% če State=final, smo končali
% sicer pokličemo step/5 in rekurzivno nadaljujemo
run(_, final, InTape, InTape).
run(Name, State, InTape, OutTape):-
  step(Name, InTape, State, NewTape, NewState),
  run(Name, NewState, NewTape, OutTape).


turing(Name, Input, Output):-
  run(Name, q0, []-Input, OutL-OutR),
  reverse(OutL, L),
  append(L, OutR, Output).