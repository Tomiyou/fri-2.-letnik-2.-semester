type status = {
  version : string;
  code : int;
} ;;

let mystatus = {
  version = "HTTP/1.1";
  code = 207;
} ;;

(* vrne npr. "HTTP/1.1 200 OK" *)
let string_of_status (s: status) : string =
  s.version ^ " " ^
  (string_of_int s.code) ^ " " ^
  match s.code with
  | 200 -> "OK"
  | 201 -> "Created"
  | 202 -> "Accepted"
  | _ -> "Unknown status"
;;

type date = {
  day : int;
  month : int;
  year : int;
} ;;

type field =
  | Server of string
  | ContentLength of int
  | Location of string
  | ContentType of string
  | TransferEncoding of string
  | Date of date
  | Expires of date
  | LastModified of date
;;

type response = {
  status : status;
  headers : field list;
  body  : string;
} ;;

let string_of_date d = 
  string_of_int d.day ^ " " ^
  string_of_int d.month ^ " " ^
  string_of_int d.year
;;

let myresponse = {
  status = mystatus;
  headers = [
    Server "nginx";
    ContentLength 17;
  ];
  body = "hello web client!";
} ;;

let string_of_field (f: field) : string =
  match f with
  | Server name -> "Server: " ^ name
  | ContentLength length -> "ContentLength: " ^ (string_of_int length)
  | Location location -> "Location: " ^ location
  | ContentType t -> "Content-Type: " ^ t
  | TransferEncoding encoding -> "Transfer-Encoding: " ^ encoding
  | Date d -> "Date: " ^ (string_of_date d)
  | Expires expires -> "Expires: " ^ (string_of_date expires)
  | LastModified modified -> "Last-Modified: " ^ (string_of_date modified)
;;

let string_of_response r =
  string_of_status r.status ^ "\r\n" ^
  String.concat "\r\n" (List.map string_of_field r.headers) ^
  "\r\n\r\n" ^ r.body
;;