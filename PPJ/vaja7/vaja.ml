let vsota_lihih n =
  let i = ref 0 in
  let v = ref 0 in
  while !i < n do
    v := !v + (2 * !i+1);
    i := !i + 1
  done ;
  !v

let vsota1 n =
  let i = ref 0 in
  let v = ref 0 in
  while !i <= n do
    v := !v + !i;
    i := !i + 1
  done ;
  !v

let fib1 n =
  let a = ref 0 in
  let b = ref 1 in
  for i = 1 to n do
    let old_b = !b in
    b := !a + !b;
    a := old_b
  done ;
  !a

let rec vsota2 n =
  match n with
  | 0 -> 0
  | _ -> n + vsota2 (n-1)

let rec fibonacci2 n =
  match n with
  | 0 -> 0
  | 1 -> 1
  | _ -> fibonacci2 (n-2) + fibonacci2 (n-1)

let vsota3 n =
  let rec loop i v =
    if i <= n then
      loop (i+1) (v + (2 * i + 1))
    else
      v
  in loop 0 0

let fibonacci3 n =
  let rec loop a b i =
    if i < n then
      loop b (a+b) (i+1)
    else
      a
  in loop 0 1 0